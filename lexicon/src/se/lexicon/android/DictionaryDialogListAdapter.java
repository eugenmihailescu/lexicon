package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		DictionaryDialogListAdapter.java $
 * 
 * @id:	DictionaryDialogListAdapter.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.ArrayList;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * A custom adapter for the application's main ListView
 * 
 */
public class DictionaryDialogListAdapter extends BaseAdapter {

	private Context context;
	private DialogInterface dialog;
	private boolean[] checked;
	private ArrayList<DictionaryFile> items;
	private boolean multiChoice;
	public android.content.DialogInterface.OnClickListener onClick;

	public DictionaryDialogListAdapter(Context context,
			ArrayList<DictionaryFile> items, boolean multiChoice) {
		this.context = context;
		this.items = items;
		checked = new boolean[items.size()];
		for (int i = 0; i < items.size(); i++)
			checked[i] = false;
		this.multiChoice = multiChoice;
	}

	public boolean getChecked(int position) {
		return checked[position];
	}

	@Override
	public int getCount() {
		if (items.size() > 0)
			return items.size();
		else
			return 0;
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).id;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.dictionary_dialog_row, null);
		}
		DictionaryFile o = items.get(position);
		if (o != null) {
			LinearLayout rowItem = (LinearLayout) v.findViewById(R.id.rowItem);
			TextView dictName = (TextView) v.findViewById(R.id.dictName);
			TextView dictType = (TextView) v.findViewById(R.id.dictType);
			TextView dictSize = (TextView) v.findViewById(R.id.dictSize);
			TextView dictObjects = (TextView) v.findViewById(R.id.dictObjects);
			final RadioButton radChecked = (RadioButton) v
					.findViewById(R.id.radChecked);
			final CheckBox chkChecked = (CheckBox) v
					.findViewById(R.id.chkChecked);

			dictName.setText(items.get(position).name);
			dictType.setText(items.get(position).type.name());
			dictSize.setText(String.valueOf(Utils.getFormatedSize(items
					.get(position).size)));
			dictObjects
					.setText(String.valueOf(items.get(position).itemCount
							+ " "
							+ (items.get(position).type == DictionaryType.WORD ? "words"
									: "files")));
			radChecked.setChecked(checked[position]);
			chkChecked.setChecked(checked[position]);
			if (multiChoice) {
				radChecked.setVisibility(View.GONE);
				chkChecked.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						setCheckedIndex(position, !getChecked(position));
						if (null != onClick)
							onClick.onClick(dialog, position);
					}
				});
			} else {
				chkChecked.setVisibility(View.GONE);
				final int idx = position;
				radChecked.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						setCheckedIndex(idx, true);
						if (null != onClick)
							onClick.onClick(dialog, idx);
					}
				});
			}
			rowItem.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (null != onClick)
						onClick.onClick(dialog, position);

				}
			});
		}
		return v;
	}

	public void setChecked(boolean[] checkedArray) {
		checked = checkedArray;
	}

	public void setCheckedIndex(int position, boolean isChecked) {
		if (position > -1) {
			checked[position] = isChecked;
			if (!multiChoice)
				for (int i = 0; i < checked.length; i++)
					checked[i] = (i == position ? isChecked : !isChecked);
		}
		notifyDataSetChanged();
	}

	public void setDialog(DialogInterface dialog) {
		this.dialog = dialog;
	}
}
