package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		Dictionary.java $
 * 
 * @id:	Dictionary.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to store all the info about a sqlite dictionary database
 * 
 */
class Dictionary {
	public int cacheSize;
	public String comment;
	public String created;
	public String last_changed;
	public String license;
	public String licenseComment;
	public String name;
	public String originURL;
	public String downloadURL;
	public String source_language;
	public String target_language;
	public String version;
	public int wordCount;
	private List<SqliteTableDef> tables;
	public List<Word> cacheWords; // words that are used could be keept in cache

	public Dictionary() {
		super();
		cacheWords = new ArrayList<Word>();
		tables = new ArrayList<SqliteTableDef>();
		cacheSize = 100;
	}

	public SqliteTableDef addTable(String tblName) {
		if (tables.add(new SqliteTableDef(tblName.toLowerCase())))
			return tables.get(tables.size() - 1);
		else
			return null;
	}

	public void addTableField(String tblName, String fldName) {
		for (int i = 0; i < tables.size(); i++) {
			SqliteTableDef t = tables.get(i);
			if (t.tblName.compareToIgnoreCase(tblName) == 0)
				t.fields.add(fldName.toLowerCase());
		}
	}

	public SqliteTableDef getTable(String tblName) {
		SqliteTableDef tmp, result = null;
		for (int i = 0; i < tables.size(); i++) {
			tmp = tables.get(i);
			if (tmp.tblName.compareToIgnoreCase(tblName) == 0) {
				result = tmp;
				break;
			}
		}
		return result;
	}

	public String getWordSqlQuery() {
		String result = "select " + "word.word_id," + "word.value,"
				+ "word.class_," + "word.lang," + "word.comment,"

				+ (tableExists("definition") ? "definition.value" : "\"\"")
				+ ","

				+ (tableExists("explanation") ? "explanation.value" : "\"\"")
				+ ","

				+ (tableExists("grammar") ? "grammar.value" : "\"\"")
				+ ","

				+ (tableExists("phonetic") ? "phonetic.value" : "\"\"")
				+ ","
				+ (tableExists("phonetic") ? "phonetic.soundFile" : "\"\"")
				+ ","

				+ (tableExists("use") ? "use.value" : "\"\"")
				+ ","

				+ (tableExists("variant") ? "variant.value" : "\"\"")
				+ ","
				+ (tableExists("variant") ? "variant.alt" : "\"\"")
				+ ","

				// col : 13
				+ (tableExists("definition") ? "definition.definition_id"
						: "-1")
				+ ","
				+ (tableExists("explanation") ? "explanation.explanation_id"
						: "-1")
				+ ","
				+ (tableExists("grammar") ? "grammar.grammar_id" : "-1")
				+ ","
				+ (tableExists("phonetic") ? "phonetic.phonetic_id" : "-1")
				+ ","
				+ (tableExists("use") ? "use.use_id" : "-1")
				+ ","
				+ (tableExists("variant") ? "variant.variant_id" : "-1")
				+ " "

				+ "from word "
				+ (tableExists("definition") ? "left outer join definition on word.word_id=definition.word_id "
						: "")
				+ (tableExists("explanation") ? "left outer join explanation on word.word_id=explanation.word_id "
						: "")
				+ (tableExists("grammar") ? "left outer join grammar on word.word_id=grammar.word_id "
						: "")
				+ (tableExists("phonetic") ? "left outer join phonetic on word.word_id=phonetic.word_id "
						: "")
				+ (tableExists("use") ? "left outer join use on word.word_id=use.word_id "
						: "")
				+ (tableExists("variant") ? "left outer join variant on word.word_id=variant.word_id "
						: "") + "where word.word_id=?;";
		// Log.println(Log.DEBUG, "getWordSqlQuery", result);
		return result;

	}

	public boolean tableExists(String tblName) {
		boolean result = false;
		for (int i = 0; i < tables.size(); i++)
			if (tables.get(i).tblName.compareToIgnoreCase(tblName) == 0) {
				result = true;
				break;
			}
		return result;
	}

	public boolean tableFieldExists(String tblName, String fldName) {
		boolean result = false;
		for (int i = 0; i < tables.size(); i++) {
			SqliteTableDef tbl = tables.get(i);
			if (tbl.tblName.compareToIgnoreCase(tblName) == 0) {
				for (int j = 0; j < tbl.fields.size(); j++)
					if (tbl.fields.get(j).compareToIgnoreCase(fldName) == 0) {
						result = true;
						break;
					}
				break;
			}
		}
		return result;
	}

	// TODO not intend to use it here but it is a good code snippet to copy one
	// object's properties to another; to be used in other place ;o)

	// public static <T> void copyFields(T target, T source) throws Exception {
	// Class<?> clazz = source.getClass();
	//
	// for (Field field : clazz.getFields()) {
	// Object value = field.get(source);
	// field.set(target, value);
	// }
	// }

}

/**
 * Class used to store one dictionary
 * 
 */
class DictionaryFile {

	/**
	 * An integer that should identify uniquely a dictionary file
	 */
	public int id;

	/**
	 * The name of the dictionary as shown to the end-user
	 */
	public String name;

	/**
	 * The type of the dictionary: words, mp3, synonyms
	 */
	public DictionaryType type;

	/**
	 * The (root) file name as the dictionary is available on the web and as it
	 * will be saved locally
	 */
	public String fileName;

	/**
	 * Number of parts that the dictionary is splitted (1 if only one part)
	 */
	public int fileSplitCount;

	/**
	 * The root URL where the dictionary parts can be found and downloaded
	 */
	public String root_url;

	/**
	 * true if the dictionary is archived (zip)
	 */
	public boolean archive;

	/**
	 * The unpacked size (in bytes) of the dictionary
	 */
	public int size;

	/**
	 * The number of items(i.e. words/audio files) that are included physically
	 * or logically in dictionary
	 */
	public int itemCount;

	public DictionaryFile() {
		this.id = -1;
		this.name = "";
		this.fileName = "";
		this.fileSplitCount = 0;
		this.root_url = "";
		this.type = DictionaryType.WORD;
		this.archive = false;
		this.size = 0;
		this.itemCount = -1;
	}

	public DictionaryFile(int id, String name, String fileName,
			int fileSplitCount, String root_url, DictionaryType type,
			boolean archive, int size, int wordCount) {
		this.id = id;
		this.name = name;
		this.fileName = fileName;
		this.fileSplitCount = fileSplitCount;
		this.root_url = root_url;
		this.type = type;
		this.archive = archive;
		this.size = size;
		this.itemCount = wordCount;
	}
}

enum DictionaryType {
	WORD, MP3, SYNONYMS,RAW
}

/**
 * Class used to store the structure of a specific table within sqlite
 * dictionary
 * 
 */
class SqliteTableDef {
	String tblName;
	List<String> fields;

	SqliteTableDef(String table) {
		tblName = table;
		fields = new ArrayList<String>();
	}

	public int addField(String fldName) {
		if (fields.add(fldName.toLowerCase().trim()))
			return fields.size() - 1;
		else
			return -1;
	}

	public boolean fieldExists(String fldName) {
		boolean result = false;
		for (int i = 0; i < fields.size(); i++)
			if (fields.get(i).compareToIgnoreCase(fldName) == 0) {
				result = true;
				break;
			}
		return result;
	}
}