package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		LiveUpdate.java $
 * 
 * @id:	LiveUpdate.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Environment;

/**
 * Class used to update the current application to the latest version available
 * on the Internet
 * 
 */
public class LiveUpdate {

	class PInfo {
		private String appname = "";
		@SuppressWarnings("unused")
		private String versionName = "";
		private int versionCode = 0;
	}

	final static int HTTP_TIMEOUT = 60000; // 1 min
	final static int DOWNLOAD_BUFFER_SIZE = 16 * 1024;

	/**
	 * Retrieve the application name for the current package
	 * 
	 * @param context
	 *            the context from where to read the string value of the
	 *            application name
	 * @return a string containing the name of the application
	 */
	public static String getAppName(Context context) {
		String versionName = "";
		try {
			versionName = context
					.getString(context.getPackageManager().getPackageInfo(
							context.getPackageName(), 0).applicationInfo.labelRes);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionName;

	}

	/**
	 * Retrieve the application package name
	 * 
	 * @param context
	 *            the context from where to read the string value of the package
	 *            name
	 * @return a string containing the name of the application's package
	 */
	public static String getPkgName(Context context) {
		String versionName = "";
		try {
			versionName = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).packageName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return versionName;
	}

	Resources resource;
	private Context context;
	private String tempApkName = "temp.apk";
	private int VersionCode;
	private String AppName;

	private String urlpath;

	private String PackageName;

	/**
	 * @param context
	 *            Context from where you call this class
	 * @param versionCode
	 *            Your current app version code
	 * @param urlpath
	 *            the url there we download the new app
	 * @throws LiveUpdException
	 */
	public LiveUpdate(Context context, int versionCode, String urlpath)
			throws LiveUpdException {
		this.context = context;
		this.resource = context.getResources();
		this.VersionCode = versionCode;
		this.AppName = getAppName(context);
		this.urlpath = urlpath;
		this.PackageName = "package:" + getPkgName(context);
	}

	// Download On My Mobile SDCard or Emulator.
	public boolean DownloadOnSDcard() throws IOException {
		boolean result = false;
		String tempDownloadPath = Environment.getExternalStorageDirectory()
				+ "/download/";
		URL url = new URL(urlpath); // Your given URL.

		HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setRequestMethod("GET");
		httpConn.setConnectTimeout(HTTP_TIMEOUT);
		httpConn.setReadTimeout(HTTP_TIMEOUT);
		httpConn.setDoOutput(true);
		httpConn.connect(); // Connection Complete here.!

		File file = new File(tempDownloadPath); // PATH = /mnt/sdcard/download/
		if (!file.exists()) {
			file.mkdirs();
		}
		File outputFile = new File(file, tempApkName);
		FileOutputStream fos = new FileOutputStream(outputFile);

		// Get from Server and Catch In Input Stream Object.
		InputStream is = httpConn.getInputStream();

		byte[] buffer = new byte[DOWNLOAD_BUFFER_SIZE];
		int bRead = 0;
		while ((bRead = is.read(buffer)) != -1) {
			fos.write(buffer, 0, bRead); // Write In FileOutputStream.
		}
		fos.close();
		is.close();

		result = true;

		return result;
	}

	/**
	 * Retrieve a complete list of all installed applications
	 * 
	 * @param getSysPackages
	 *            if true then retrieve also the system packages
	 * @return an array of PInfo containing the list of the installed
	 *         applications
	 */
	private ArrayList<PInfo> getInstalledApps(boolean getSysPackages) {
		ArrayList<PInfo> res = new ArrayList<PInfo>();
		List<PackageInfo> packs = context.getPackageManager()
				.getInstalledPackages(0);

		for (int i = 0; i < packs.size(); i++) {
			PackageInfo p = packs.get(i);
			if ((!getSysPackages) && (p.versionName == null)) {
				continue;
			}
			PInfo newInfo = new PInfo();
			newInfo.appname = p.applicationInfo.loadLabel(
					context.getPackageManager()).toString();
			newInfo.versionName = p.versionName;
			newInfo.versionCode = p.versionCode;
			res.add(newInfo);
		}
		return res;
	}

	/**
	 * Shows the android install screen that will install the current downloaded
	 * .apk file
	 * 
	 * @return true if it was installed successfully
	 */
	public boolean InstallApplication() {
		Uri packageURI = Uri.parse(PackageName);
		Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
				packageURI);

		intent.setDataAndType(
				Uri.fromFile(new File(Environment.getExternalStorageDirectory()
						+ "/download/" + tempApkName)),
				"application/vnd.android.package-archive");

		context.startActivity(intent);
		return true;
	}

	/**
	 * Check for the specified appName if there is an available update out there
	 * (a dummy implementation : it compares the installed application
	 * versionCode with the one on found on the Internet)
	 * 
	 * @param appName
	 *            the application name to be checked
	 * @return true if current installed application versionCode is smaller than
	 *         the one available on the Internet
	 */
	private Boolean isUpdateAvailable(String appName) {
		Boolean isAvailable = false;
		ArrayList<PInfo> apps = getInstalledApps(false);// F= no system packages
		final int max = apps.size();
		for (int i = 0; i < max; i++) {
			if (apps.get(i).appname.equalsIgnoreCase(appName)) {
				if (VersionCode <= apps.get(i).versionCode) {
					isAvailable = true;
					break;
				}
			}
		}
		return isAvailable;
	}

	/**
	 * The method that should be called to run this class
	 * 
	 * @throws LiveUpdException
	 * @throws IOException
	 */
	public void run() throws LiveUpdException, IOException {
		if (!(isUpdateAvailable(AppName) && DownloadOnSDcard()
				&& UnInstallApplication(PackageName) && InstallApplication()))
			throw new LiveUpdException(String.format(
					resource.getString(R.string.class_liveupdate_runexception),
					AppName));
	}

	/**
	 * Shows the android's uninstall screen which propose to uninstall the
	 * current application
	 * 
	 * @param packageName
	 *            package name to be uninstalled
	 * @return true if package uninstalled successfully
	 */
	public boolean UnInstallApplication(String packageName) {
		Uri packageURI = Uri.parse(packageName);
		Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
		context.startActivity(uninstallIntent);
		return true;
	}
}

/**
 * Custom exception class used in case of a particular LiveUpdate exception (see
 * run)
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class LiveUpdException extends Exception {
	private static final long serialVersionUID = -1878031797102837675L;

	public LiveUpdException(String detailMessage) {
		super(detailMessage);
	}

}
