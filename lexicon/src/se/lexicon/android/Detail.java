package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		Detail.java $
 * 
 * @id:	Detail.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.text.util.Linkify.TransformFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Detail extends Activity {

	// TODO android pinch zoom

	// provide a image resource based on the fake image source specified in the
	// img tag (cound be even external)
	private class ImageGetter implements Html.ImageGetter {

		@Override
		public Drawable getDrawable(String source) {
			int id;

			if (source.equals("divider_horizontal_dark")) {
				id = android.R.drawable.divider_horizontal_dark;
			}
			if (source.equals("phonetic")) {
				id = R.drawable.ic_volume_pressed;
			} else {
				return null;
			}

			Drawable d = getResources().getDrawable(id);
			d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
			return d;
		}
	}

	private TextView d_html;

	/**
	 * Linkify those words that contains Picture English/Swedish
	 * 
	 * @param txtView
	 * @param word
	 */
	private void handlePictureLinks(TextView txtView, final Word word) {
		// TODO make the pattern to not match other stuff in the html text
		final String pictureUrl = "http://lexin.nada.kth.se/lang/trio/";
		TransformFilter pictureTransformer = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {

				return pictureUrl
						+ (match.group().endsWith("English") ? "en" : "sv")
						+ "/" + word.picture.get(0).value;
			}
		};

		Linkify.addLinks(txtView, Pattern.compile("[P]icture [A-Za-z]+ish"),
				null, null, pictureTransformer);
	}

	/**
	 * Linkify those words that contains Saldo/association/inflection
	 * 
	 * @param txtView
	 * @param word
	 */
	private void handleSaldoLinks(TextView txtView, final Word word) {
		// TODO make the pattern to not match other stuff in the html text
		final String[] saldoVariants = { "Saldo", "association", "inflection" };
		final String[] saldoUrl = { "fl", "lid", "lid" };
		Pattern saldoPattern = Pattern
				.compile("[S]aldo[0-3]\\w*|[a]ssociation[0-3]\\w*|[i]nflection[0-3]\\w*");
		String saldoBaseUrl = "http://spraakbanken.gu.se/ws/saldo-ws/";

		TransformFilter saldoTransform = new TransformFilter() {
			@Override
			public String transformUrl(Matcher match, String url) {
				String result = url;

				int j = Integer.parseInt(url.substring(url.length() - 1));
				String[] seeItem = word.see.get(j).value.split("\\|\\|");
				for (int i = 0; i < saldoVariants.length; i++)
					if (url.startsWith(saldoVariants[i])) {
						result = saldoUrl[i] + "/html/" + seeItem[i];
						break;
					}
				return result;
			}
		};

		Linkify.addLinks(txtView, saldoPattern, saldoBaseUrl, null,
				saldoTransform);

	};

	/**
	 * Linkify those parts of html text that contains mp3:play
	 * 
	 * @param txtView
	 * @param word
	 */
	private void handleSoundLinks(TextView txtView, final Word word) {
		// TODO make the pattern to not match other stuff in the html text

		TransformFilter tf = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {
				return "";// return exactly mp3_play://<soundFile>, nothing else
			}
		};

		// the mp3:play words are linkified and Detail(this) activity has
		// defined a special intent-filter(see .manifest) instructed to handle
		// the mp3_play:// schema (see code below, onCreate)

		Linkify.addLinks(txtView, Pattern.compile("mp3:play"), "mp3_play://"
				+ String.format("%d.%d.mp3", word.word_id, word.phonetic.id),
				null, tf);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.details);
		Button btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});

		Uri data = getIntent().getData();
		Resources res = getResources();
		// handle the mp3:play:// schema
		if (data != null && data.getScheme() != null)
			if (data.getScheme().compareTo("mp3_play") == 0) {
				Utils.playMedia(this,
						Utils.getDbDirectory(this)
								+ Utils.MP3_LOCATION
								+ (data.getSchemeSpecificPart()
										.startsWith("//") == true ? data
										.getSchemeSpecificPart().substring(2)
										: null));
				finish();
				return;
			} else {

				// schema handler is not implemented by this intent so we don't
				// know what to do
				Utils.lexToast(this, String.format(
						res.getString(R.string.activity_detail_linkhandler),
						data.getScheme()));
				finish();
				return;
			}

		ImageGetter imgGetter = new ImageGetter();
		Bundle bundle = getIntent().getExtras();
		if (bundle == null) {
			finish();
			return;
		}

		final long start = bundle.getLong("se.lexicon.android.start_time");
		final Word word = bundle.getParcelable("se.lexicon.android.detail");
		if (word == null) {
			finish();
			return;
		}

		setTitle(bundle.getString("title"));

		boolean isOnline = Utils.isOnline(Detail.this);

		d_html = ((TextView) findViewById(R.id.d_html));

		float end = (float) ((System.currentTimeMillis() - start) / 1000.0);

		// check if all sections are printed, if not then drop a line
		Set<PrintOptions> options = Utils
				.getPrintOptions(getApplicationContext());
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < PrintOptions.values().length; i++)
			if (!options.contains(PrintOptions.values()[i]))
				buff.append(PrintOptions.values()[i].name().toLowerCase()
						+ (i + 1 < PrintOptions.values().length ? "," : ""));

		if (buff.length() > 0)
			buff.insert(0,
					res.getString(R.string.activity_detail_hiddensection));

		d_html.setText(Html.fromHtml(
				word.toHtml(
						this,
						isOnline,
						null,
						HtmlUtils.quote(buff.toString())
								+ HtmlUtils.br
								+ "<div align=\"center\">"
								+ HtmlUtils.underline(String.format(
										HtmlUtils.font(
												res.getString(R.string.activity_detail_loadstatus),
												FontColor.black), end))
								+ "</div>", Utils
								.getPrintOptions(getApplicationContext())),
				imgGetter, null));

		// process Saldo links
		if (word.see != null && word.see.size() > 0
				&& word.see.get(0).value.length() > 0)
			handleSaldoLinks(d_html, word);

		// process Picture links
		if (word.picture != null && word.picture.size() > 0)
			handlePictureLinks(d_html, word);

		// process phoetic(sound) linls
		if (word.phonetic != null && word.phonetic.soundFile != null
				&& word.phonetic.soundFile.length() > 0)
			handleSoundLinks(d_html, word);
	}
}
