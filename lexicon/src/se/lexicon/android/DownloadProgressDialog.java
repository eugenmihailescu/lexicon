package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		DownloadProgressDialog.java $
 * 
 * @id:	DownloadProgressDialog.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Class used to display a download progress dialog
 * 
 */
public class DownloadProgressDialog extends Dialog {

	private class LauncherThread extends Thread {
		@Override
		public void run() {
			progressHandler.sendMessage(progressHandler.obtainMessage());
			progressHandler.sendEmptyMessage(0);
		}
	}

	private long timer;
	private Resources resource;

	private static final int KB = 1024;
	private static final int MB = KB * KB;
	private static final int GB = KB * MB;

	private static final int TITLE_SPINNER = 112;

	private static final int PROGRESS2_UPDATE = 111;
	private static final int PROGRESS1_UPDATE = 110;
	private static final int MAX2_UPDATE = 109;
	private static final int MAX1_UPDATE = 108;

	private static final int TEXT2_UPDATE = 107;
	private static final int TEXT1_UPDATE = 106;
	private static final int TITLE2_UPDATE = 105;
	private static final int TITLE1_UPDATE = 104;

	private static final int STYLE_UPDATE = 103;
	private static final int TITLE_UPDATE = 102;
	private static final int STATUS_COMPLETE = 100;

	private static final int LAYOUT_CURRENT_VISIBILITY = 113;
	private static final int LAYOUT_OVERALL_VISIBILITY = 114;

	ProgressBar progress_current, progress_overall, progress_spin;

	TextView title_current, title_overall, title_dialog, title_spinner;
	TextView percent_current, percent_overall;

	LinearLayout layout_spinner;
	LinearLayout layout_horizontal;
	LinearLayout layout_current;
	LinearLayout layout_overall;

	private int threadCount;

	private String title;

	private int style;

	private String textCurrent;

	private String textOverall;

	private String titleCurrent;

	private String titleOverall;

	private String titleSpinner;

	private int maxCurrent;

	private int maxOverall;

	private int progressCurrent;

	private int progressOverall;

	private int layout_current_visibility;
	private int layout_overall_visibility;

	/**
	 * This receive the dialog messages and based on those messages calls the
	 * internal methods to handle that specific message
	 */
	Handler progressHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			switch (msg.what) {

			case TITLE_UPDATE:
				if (null != title && title.length() > 0) {
					title_dialog.setText(title);
					title_dialog.setVisibility(View.VISIBLE);
				} else
					title_dialog.setVisibility(View.GONE);
				break;
			case STYLE_UPDATE:
				switch (style) {
				case ProgressDialog.STYLE_HORIZONTAL:
					layout_horizontal.setVisibility(View.VISIBLE);
					layout_spinner.setVisibility(View.GONE);
					break;
				case ProgressDialog.STYLE_SPINNER:
					layout_horizontal.setVisibility(View.GONE);
					layout_spinner.setVisibility(View.VISIBLE);
					break;
				default:
					break;
				}
				break;
			case TITLE_SPINNER:
				title_spinner.setText(titleSpinner);
				break;
			case PROGRESS2_UPDATE:
				progress_overall.setProgress(progressOverall);
				break;
			case PROGRESS1_UPDATE:
				progress_current.setProgress(progressCurrent);
				break;
			case MAX2_UPDATE:
				progress_overall.setMax(maxOverall);
				break;
			case MAX1_UPDATE:
				progress_current.setMax(maxCurrent);
				break;
			case TEXT2_UPDATE:
				if (null != textOverall && textOverall.length() > 0) {
					percent_overall.setText(textOverall);
					percent_overall.setVisibility(View.VISIBLE);
				} else
					percent_overall.setVisibility(View.GONE);
				break;
			case TEXT1_UPDATE:
				if (null != textCurrent && textCurrent.length() > 0) {
					percent_current.setText(textCurrent);
					percent_current.setVisibility(View.VISIBLE);
				} else
					percent_current.setVisibility(View.GONE);
				break;
			case TITLE2_UPDATE:
				if (null != titleOverall && titleOverall.length() > 0) {
					title_overall.setText(titleOverall);
					title_overall.setVisibility(View.VISIBLE);
				} else
					title_overall.setVisibility(View.GONE);
				break;
			case TITLE1_UPDATE:
				if (null != titleCurrent && titleCurrent.length() > 0) {
					title_current.setText(titleCurrent);
					title_current.setVisibility(View.VISIBLE);
				} else
					title_current.setVisibility(View.GONE);
				break;
			case LAYOUT_CURRENT_VISIBILITY:
				layout_current.setVisibility(layout_current_visibility);
				break;
			case LAYOUT_OVERALL_VISIBILITY:
				layout_overall.setVisibility(layout_overall_visibility);
				break;
			case STATUS_COMPLETE:
				dismiss();
			}
		}
	};

	/**
	 * Constructor of the dialog class
	 * 
	 * @param context
	 *            the context where to create and show the dialog box
	 */
	public DownloadProgressDialog(Context context) {
		super(context);
		this.resource = context.getResources();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.downloadprogressbar);
		prepareDialog();
	}

	/**
	 * Decrements the internal number of threads by 1
	 */
	public void decrementThreads() {
		this.threadCount--;
	}

	private float getCurrentDone() {
		return maxCurrent > 0 ? 100 * (float) progressCurrent / maxCurrent : 0;
	}

	/**
	 * @return a string containing the percentage done of the current
	 *         downloading file
	 */
	private String getCurrentDoneStr() {
		return String.format("%.2f%% %s (%s)", getCurrentDone(),
				resource.getString(R.string.dialog_dwlprogress_done),
				getCurrentSizeStr());
	}

	/**
	 * @return the formated size of current downloading file
	 */
	private String getCurrentSizeStr() {
		if ((progressCurrent / KB) < KB)
			return String.format("%.2f KB", (float) (progressCurrent / KB));
		else if ((progressCurrent / MB) < MB)
			return String.format("%.2f MB", (float) progressCurrent / MB);
		else if ((progressCurrent / GB) < GB)
			return String.format("%.2f GB", (float) progressCurrent / GB);
		else
			return String.format("%.2f TB", (float) progressCurrent / KB * GB);
	}

	/**
	 * Format a file size specified in bytes and return the corresponding value
	 * in KB/MB/GB/TB
	 * 
	 * @param n
	 *            the size of bytes to be formated
	 * @return a string containing the formated size
	 */
	private String getFormatedSize(float n) {
		if ((n / KB) < KB)
			return String.format("%.2f KB", n / KB);
		else if ((n / MB) < MB)
			return String.format("%.2f MB", n / MB);
		else if ((n / GB) < GB)
			return String.format("%.2f GB", n / GB);
		else
			return String.format("%.2f TB", n / KB * GB);
	}

	/**
	 * @return the max progress for the currently downloaded file
	 */
	public int getMaxProgCurrent() {
		return maxCurrent;
	}

	/**
	 * @return return the max progress for the overall downloaded files
	 */
	public int getMaxProgOverall() {
		return maxOverall;
	}

	/**
	 * @return the percentage of the overall download done so far
	 */
	private float getOverallDone() {
		return maxOverall > 0 ? 100 * (float) progressOverall / maxOverall : 0;
	}

	/**
	 * @return a string containing the percentage done + ETA
	 */
	private String getOverallDoneStr() {
		return String.format("%.2f%% %s %s (%s)  ETA %s", getOverallDone(),
				resource.getString(R.string.dialog_dwlprogress_done),
				getCurrentDone(), getOverallSizeStr(), getOverallETA());
	}

	/**
	 * @return a string containing an approximation of the Estimated Time
	 *         Arrival of the download finish
	 */
	private String getOverallETA() {
		final float end = getSpentTime();
		final float total = (maxOverall * end / progressOverall) - end;
		final int hour = (int) (total / 3600);
		final int min = (int) ((total - hour * 3600) / 60);
		final int sec = (int) (total - hour * 3600 - min * 60);

		if (progressOverall > 0)
			return String.format("%02d:%02d:%02d", hour, min, sec);
		else
			return "N/A";
	}

	/**
	 * @return a formated string containing the overall size of download so far
	 */
	private String getOverallSizeStr() {
		return getFormatedSize(progressOverall);
	}

	/**
	 * @return the number of seconds spent in average for the all threads from
	 *         the beginning until now
	 */
	public float getOverallSpeed() {
		final float total = getSpentTime();
		if (total > 0)
			return progressOverall / total;
		else
			return 0;
	}

	/**
	 * @return a formated string containing the average download speed
	 *         (calculated from the beggining of the download)
	 */
	public String getOverallSpeedStr() {
		return getFormatedSize(getOverallSpeed());
	}

	/**
	 * @return a string containing the default text to be be shown above the
	 *         overall progress bar
	 */
	private String getOverallTitle() {
		return String.format(
				resource.getString(R.string.dialog_dwlprogress_overalltitle),
				getThreadCount(), getOverallSpeedStr());
	}

	/**
	 * @return the number of seconds spent from the moment that timer was
	 *         started (i.e. at dialog initialization)
	 */
	private float getSpentTime() {
		return (float) ((System.currentTimeMillis() - timer) / 1000.0);
	}

	/**
	 * @return the number of the active threads
	 */
	public int getThreadCount() {
		return this.threadCount;
	}

	/**
	 * Increments the internal number of threads by 1
	 */
	public void incrementThreads() {
		this.threadCount++;
	}

	/**
	 * Start the timer, set the initial value, launch the thread used to show
	 * the progrss bar dialog
	 */
	private void prepareDialog() {
		threadCount = 0;
		startTimer();

		layout_spinner = (LinearLayout) findViewById(R.id.layout_spinner);
		layout_horizontal = (LinearLayout) findViewById(R.id.layout_horizontal);

		layout_current = (LinearLayout) findViewById(R.id.layout_current);
		layout_overall = (LinearLayout) findViewById(R.id.layout_overall);

		progress_spin = (ProgressBar) findViewById(R.id.progress_spin);
		title_spinner = (TextView) findViewById(R.id.title_spinner);
		title_dialog = (TextView) findViewById(R.id.title_dialog);

		progress_current = (ProgressBar) findViewById(R.id.progress_current);
		progress_overall = (ProgressBar) findViewById(R.id.progress_overall);

		title_current = (TextView) findViewById(R.id.title_current);
		percent_current = (TextView) findViewById(R.id.percent_current);

		title_overall = (TextView) findViewById(R.id.title_overall);
		percent_overall = (TextView) findViewById(R.id.percent_overall);

		progress_current.setIndeterminate(false);
		progress_overall.setIndeterminate(false);

		setProgressCurrent(0);
		setProgressOverall(0);
		setTitle("");

		new LauncherThread().start();
	}

	/**
	 * Increments the current file progress bar by the <increment> specified
	 * 
	 * @param increment
	 *            the step by which the progress will be incremented
	 */
	public void setIncrementCurrent(int increment) {
		this.progressCurrent += increment;
		progressHandler.sendEmptyMessage(PROGRESS1_UPDATE);
		setTextCurrent(getCurrentDoneStr());
		setIncrementOverall(increment);
	}

	/**
	 * Increments the overall progress bar by the <increment> specified
	 * 
	 * @param increment
	 *            the step by which the progress will be incremented
	 */
	public void setIncrementOverall(int increment) {
		this.progressOverall += increment;
		progressHandler.sendEmptyMessage(PROGRESS2_UPDATE);
		setTextOverall(getOverallDoneStr());
		setTitleOverall(getOverallTitle());
	}

	/**
	 * Set the maximum value of the current file progress bar
	 * 
	 * @param max1
	 *            the value of the max
	 */
	public void setMaxProgCurrent(int max1) {
		this.maxCurrent = max1;
		progressHandler.sendEmptyMessage(MAX1_UPDATE);
	}

	/**
	 * Set the maximum value of the overall progress bar
	 * 
	 * @param max2
	 *            the value of the max
	 */
	public void setMaxProgOverall(int max2) {
		this.maxOverall = max2;
		progressHandler.sendEmptyMessage(MAX2_UPDATE);
	}

	/**
	 * Set the value of the current file progress
	 * 
	 * @param progress1
	 *            the value of the progress
	 */
	public void setProgressCurrent(int progress1) {
		this.progressCurrent = progress1;
		progressHandler.sendEmptyMessage(PROGRESS1_UPDATE);
		setTextCurrent(getCurrentDoneStr());
		setProgressOverall(this.progressOverall + progress1);
	}

	/**
	 * Set the value of the overall progress
	 * 
	 * @param progress2
	 *            the value of the progress
	 */
	public void setProgressOverall(int progress2) {
		this.progressOverall = progress2;
		progressHandler.sendEmptyMessage(PROGRESS2_UPDATE);
		setTextOverall(getOverallDoneStr());
		setTitleOverall(getOverallTitle());
	}

	/**
	 * Set the style of the progress bar (STYLE_HORIZONTAL|STYLE_SPINNER)
	 * 
	 * @param style
	 *            an integer value corresponding to the style to be used
	 */
	public void setStyle(int style) {
		this.style = style;
		progressHandler.sendEmptyMessage(STYLE_UPDATE);
	}

	/**
	 * Set the text bellow the current file progress bar
	 * 
	 * @param text1
	 *            text to be shown
	 */
	public void setTextCurrent(String text1) {
		this.textCurrent = text1;
		progressHandler.sendEmptyMessage(TEXT1_UPDATE);
	}

	/**
	 * Set the text bellow the overall progress bar
	 * 
	 * @param text2
	 *            to be shown
	 */
	public void setTextOverall(String text2) {
		this.textOverall = text2;
		progressHandler.sendEmptyMessage(TEXT2_UPDATE);
	}

	/*
	 * Shows the dialog title (make sense only in the case of a HORIZONTAL
	 * progress dialog) (non-Javadoc)
	 * 
	 * @see android.app.Dialog#setTitle(java.lang.CharSequence)
	 */
	@Override
	public void setTitle(CharSequence title) {
		this.title = title.toString();
		progressHandler.sendEmptyMessage(TITLE_UPDATE);
	}

	/**
	 * Set the text above the current file progress bar
	 * 
	 * @param title1
	 *            the text to be shown
	 */
	public void setTitleCurrent(String title1) {
		this.titleCurrent = title1;
		progressHandler.sendEmptyMessage(TITLE1_UPDATE);
	}

	/**
	 * Set the text above the overall progress bar
	 * 
	 * @param title2
	 *            the text to be shown
	 */
	public void setTitleOverall(String title2) {
		this.titleOverall = title2;
		progressHandler.sendEmptyMessage(TITLE2_UPDATE);
	}

	/**
	 * Set the title in the case of a spinner dialog type
	 * 
	 * @param titleSpinner
	 *            the title to be shown
	 */
	public void setTitleSpinner(String titleSpinner) {
		this.titleSpinner = titleSpinner;
		progressHandler.sendEmptyMessage(TITLE_SPINNER);
	}

	/**
	 * Set the current file progress bar visibility
	 * 
	 * @param visibility
	 *            VISIBLE|INVISIBLE|GONE
	 */
	public void setVisibilityCurrent(int visibility) {
		this.layout_current_visibility = visibility;
		progressHandler.sendEmptyMessage(LAYOUT_CURRENT_VISIBILITY);
	}

	/**
	 * Set the overall progress bar visibility
	 * 
	 * @param visibility
	 *            VISIBLE|INVISIBLE|GONE
	 */
	public void setVisibilityOverall(int visibility) {
		this.layout_overall_visibility = visibility;
		progressHandler.sendEmptyMessage(LAYOUT_OVERALL_VISIBILITY);
	}

	/**
	 * Used internally to start counting when the download started
	 */
	private void startTimer() {
		this.timer = System.currentTimeMillis();
	}
}