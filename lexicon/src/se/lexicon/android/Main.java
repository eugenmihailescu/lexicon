package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		Main.java $
 * 
 * @id:	Main.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;

/**
 * The class that knows how to handle the XML attributes
 * 
 */
class DictionarySaxParser extends DefaultHandler {

	List<DictionaryFile> dictionaries;

	@Override
	public void startDocument() throws SAXException {
		dictionaries = new ArrayList<DictionaryFile>();
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attrs) throws SAXException {

		if (qName.compareToIgnoreCase("dictionary") == 0) {
			DictionaryFile dictionary = new DictionaryFile();
			dictionary.archive = Boolean
					.parseBoolean(attrs.getValue("archive"));
			dictionary.fileName = attrs.getValue("file");
			dictionary.fileSplitCount = Integer.parseInt(attrs
					.getValue("volumes"));
			dictionary.name = attrs.getValue("name");
			dictionary.itemCount = Integer.parseInt(attrs.getValue("items"));
			dictionary.root_url = attrs.getValue("url");
			dictionary.size = Integer.parseInt(attrs.getValue("size"));
			dictionary.type = DictionaryType.valueOf(attrs.getValue("type"));
			dictionaries.add(dictionary);
		}
	}
}

interface IAfterExecute {
	void onDone(Object... params);
}

interface IOnDownloadCanceled {
	boolean isCanceled();
}

interface IProgressUpdater {
	void setMaxPosition(int maxpos);

	void setProgress(int position, String details, long start);

	void setStatus(String status);
}

/**
 * Class used to store the items of the list adapted
 */
class LexiconAdapterItem {

	int word_id;

	String value;

	String translation;

	public LexiconAdapterItem(int word_id, String value, String translation) {
		this.word_id = word_id;
		this.value = value;
		this.translation = translation;
	}

	public String getTranslation() {
		return translation;
	}

	public String getValue() {
		return value;
	}

	public int getWord_id() {
		return word_id;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setWord_id(int word_id) {
		this.word_id = word_id;
	}

}

/**
 * Application Main class
 * 
 * @author Eugen Mihailescu
 */
public class Main extends ListActivity implements Shaker.Callback {

	/**
	 * Class used to run asynchronously the code that take care of loading the
	 * detail page
	 * 
	 * @author "Eugen Mihailescu"
	 * 
	 */
	private class AsyncTaskDetailLoader extends AsyncTask<Void, Integer, Void> {

		private Intent detail;

		private int word_id;

		private Set<PrintOptions> options;
		private Main activity;
		private boolean completed;

		public AsyncTaskDetailLoader(Intent detail, int word_id,
				Set<PrintOptions> options, Main activity) {
			this.detail = detail;
			this.word_id = word_id;
			this.options = options;
			this.activity = activity;
		}

		@Override
		protected Void doInBackground(Void... params) {
			final long start = System.currentTimeMillis();
			Word word = getWord(word_id, options, new IProgressUpdater() {

				@Override
				public void setMaxPosition(int maxpos) {
					wordDetailDlg.setMax(maxpos);
				}

				@Override
				public void setProgress(int position, String details, long start) {
					wordDetailDlg.setProgress(position);

					// TODO implementare afisare details
					// in momentul asta thread-ul da eroare la intreruperea lui
					// pt a afisa acest mesaj (?!); poate cu un layout custom...
				}

				@Override
				public void setStatus(String status) {
					// TODO Auto-generated method stub
				}
			});
			detail.putExtra("se.lexicon.android.start_time", start);
			detail.putExtra("se.lexicon.android.detail", word);
			detail.putExtra("title", getString(R.string.app_title) + "-"
					+ getString(R.string.menu_detail).toLowerCase());
			startActivity(detail);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			completed = true;
			if (null != activity) {
				wordDetailDlg.setProgress(0);
				activity.onAsyncTaskDetailLoaderDone(word_id);
			}
		}

		@Override
		protected void onPreExecute() {
			activity.showDialog(LOADING_DIALOG);
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			if (null != activity)
				wordDetailDlg.setProgress(progress[0]);
		}

		public void setActivity(Main activity) {
			this.activity = activity;
		}
	}

	/**
	 * Class used to run asynchronously the code that take care of downloading a
	 * file from internet
	 * 
	 * @author "Eugen Mihailescu"
	 * 
	 */
	private class AsyncTaskDownloader extends AsyncTask<Void, Integer, Void> {

		private ArrayList<String> parts;

		private String outputFile;

		private boolean completed;

		private IOException exception;
		private Main activity;
		private int threadsMax;
		private boolean canceled;
		private DictionaryFile dictFile;

		/**
		 * Creates new asynchronous download thread that will download and
		 * install the dictionary specified by parts and type
		 * 
		 * @param activity
		 *            the activity whose UI context will be used/updated
		 * @param dictFile
		 *            the dictionary file object that contains the necessary
		 *            info to know what/how to download
		 * @param parts
		 *            a list of String that represents one or more files to be
		 *            downloaded
		 * @param outputPath
		 *            the local output path where to store the parts that will
		 *            be downloaded
		 * @param maxProgress
		 *            specify the max value for the overall progress dialog
		 */
		public AsyncTaskDownloader(Main activity, DictionaryFile dictFile,
				ArrayList<String> parts, String outputPath, int maxProgress) {
			this.parts = parts;
			this.outputFile = outputPath + dictFile.fileName;
			this.activity = activity;
			this.threadsMax = 0;
			this.canceled = false;
			this.dictFile = dictFile;
		}

		@Override
		protected Void doInBackground(Void... params) {
			exception = null;
			Resources res = activity.getResources();
			try {
				// estimating
				downloadDlg.setStyle(ProgressDialog.STYLE_SPINNER);
				downloadDlg.setTitle("");
				downloadDlg.setTitleSpinner(res
						.getString(R.string.main_dwl_estimsize));

				int tmpSize = Utils.getDownloadSize(dictFile.root_url, parts,
						dictFile.archive);
				long freeSpace = Utils.getSDCardFreeSpace();
				if (2 * tmpSize > freeSpace)
					throw new IOException(String.format(
							res.getString(R.string.main_dwl_sdcardfull),
							Utils.getFormatedSize(freeSpace),
							Utils.getFormatedSize(2 * tmpSize)));

				vMaxDownload += tmpSize;
				threadsMax = threadsMax > vMaxDownload ? threadsMax
						: vMaxDownload;

				downloadDlg.setProgressCurrent(0);
				if (downloadDlg.getThreadCount() == 0)
					downloadDlg.setProgressOverall(0);

				downloadDlg.incrementThreads();

				// downloading
				downloadDlg.setStyle(ProgressDialog.STYLE_HORIZONTAL);
				downloadDlg.setTitle(res.getString(R.string.main_dwl_wait));

				Utils.DownloadOnSDcard(activity, appPref.dwlBufferSize * 1024,
						dictFile.root_url, parts, outputFile, dictFile.archive,
						new IProgressUpdater() {

							@Override
							public void setMaxPosition(int maxpos) {
							}

							@Override
							public void setProgress(int position,
									String details, long start) {
								activity.setDownloadProgress(position,
										threadsMax);
							}

							@Override
							public void setStatus(String status) {
								activity.setDownloadStatus(status);

							}
						}, new IOnDownloadCanceled() {

							@Override
							public boolean isCanceled() {
								return canceled;
							}
						});
			} catch (IOException e) {
				exception = e;
			}
			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			canceled = true;
			Utils.lexToast1(activity,
					getResources().getString(R.string.dialog_dwl_canceled),
					android.R.drawable.stat_sys_warning);
		}

		@Override
		protected void onPostExecute(Void result) {
			completed = true;
			if (null != activity)
				activity.onAsyncTaskDownloaderDone(dictFile, outputFile,
						parts.size(), exception);
		}

		@Override
		protected void onPreExecute() {
			activity.showDialog(DOWNLOAD_DIALOG);
		}

		@Override
		protected void onProgressUpdate(Integer... progress) {
			if (null != activity)
				downloadDlg.setIncrementCurrent(progress[0]);
		}

		public void setActivity(Main activity) {
			this.activity = activity;
		}
	}

	private static final int PICK_FROM_CAMERA = 1001;
	private static final int CROP_FROM_CAMERA = 1002;
	private static final int PICK_FROM_FILE = 1003;

	private Uri mImageCaptureUri;

	// application predefined private constants
	private static final String DOWNLOAD_HOST = "https://www.dropbox.com";
	private static final String DOWNLOAD_ROOT = "http://dl.dropbox.com/u/80414534";
	private static final int LOADING_DIALOG = 0;
	private static final int DOWNLOAD_DIALOG = 1;

	// application private variables
	private AsyncTaskDetailLoader asyncWordFetchProgress;
	private AsyncTaskDownloader asynkDownloadProgress;

	private AppPreferences appPref;
	private boolean isDetailProgressDlgVisible;
	private boolean isDownloadProgressDlgVisible;

	// set to true if you want to have a TEST CASE button in the lower part of
	// the application. Then you can run the test cases which are supposed to
	// simulate and to capture the application errors
	boolean _ENABLE_TEST_CASE = false;

	// refference to the currently used dictionary
	private Dictionary currentDictionary;

	// main screen controls
	private EditText edtSearch;

	private TextView txtSearchStatus;

	private Button btnSearch;
	private Button btnClearAll;
	private Button btnMask;
	private SQLiteDatabase currentDb = null;
	private String searchPattern = "";

	private int foundWords = -1;

	private int vMaxDownload;
	private ProgressDialog wordDetailDlg;
	private DownloadProgressDialog downloadDlg;
	private Shaker shaker = null;

	private ArrayList<LexiconAdapterItem> listAdapter;

	private DictionaryListAdapter dictListAdapter;

	/**
	 * Destroy the shaker object
	 */
	private void destroyShaker() {
		if (null != shaker)
			shaker.close();
	}

	/**
	 * This method will launch Android camera crop activity for the file
	 * specified by mImageCaptureUri
	 * 
	 * The activity result will be communicated to the
	 * {@link #onActivityResult(int, int, Intent)} method
	 */
	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		if (size == 0) {
			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);

			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	/**
	 * If the bitmapPath file is not oriented normally then this method rotates
	 * the image file supplied by bitmapPath argument then returns its Bitmap
	 * object, otherwise just returns its Bitmap object
	 * 
	 * @param bitmapPath
	 *            the path to the image that has to be rotated
	 * @return a Bitmap object for the processed file
	 * @throws IOException
	 */
	private Bitmap fixPhoto(String bitmapPath) throws IOException {
		Bitmap bitmap = BitmapFactory.decodeFile(bitmapPath, null);

		ExifInterface exif = new ExifInterface(bitmapPath);
		int exifOrientation = exif
				.getAttributeInt(ExifInterface.TAG_ORIENTATION,
						ExifInterface.ORIENTATION_NORMAL);

		int rotate = 0;

		switch (exifOrientation) {
		case ExifInterface.ORIENTATION_ROTATE_90:
			rotate = 90;
			break;
		case ExifInterface.ORIENTATION_ROTATE_180:
			rotate = 180;
			break;
		case ExifInterface.ORIENTATION_ROTATE_270:
			rotate = 270;
			break;
		}

		if (rotate != 0) {
			int w = bitmap.getWidth();
			int h = bitmap.getHeight();

			// Setting pre rotate
			Matrix mtx = new Matrix();
			mtx.preRotate(rotate);

			// Rotating Bitmap & convert to ARGB_8888, required by tess
			bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
		}
		bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
		return bitmap;
	}

	/**
	 * Read the dictionary table from the sqDb sqlite connection
	 * 
	 * @param sqlDb
	 *            a working connection to the sqlite database
	 * @return a <Dictionary> structure based on the info found on database
	 */
	private Dictionary getDictionaryOnCurrentDb(SQLiteDatabase sqlDb) {
		Dictionary result = new Dictionary();
		if (sqlDb != null && sqlDb.isOpen()) {
			// add to dictionary the info taken from dictionary table
			try {
				Cursor c = sqlDb
						.rawQuery(
								"select comment,created,last_changed,license,licenseComment,name,"
										+ "originURL,source_language,target_language,version ,(select count(word_id) from word) as wordCount"
										+ " from dictionary limit 1;", null);
				c.moveToFirst();
				try {
					result.comment = c.getString(0);
					result.created = c.getString(1);
					result.last_changed = c.getString(2);
					result.license = c.getString(3);
					result.licenseComment = c.getString(4);
					result.name = c.getString(5);
					result.originURL = c.getString(6);
					result.source_language = c.getString(7);
					result.target_language = c.getString(8);
					result.version = c.getString(9);
					result.wordCount = c.getInt(10);
				} finally {
					c.close();
				}

				SqliteTableDef tbl;
				Cursor c1;

				// add to dictionary tables names as they are physicaly stored
				// on
				// sqlite database
				c = sqlDb
						.rawQuery(
								"select name from sqlite_master where type=\"table\" order by name;",
								null);
				c.moveToFirst();
				try {
					while (!c.isAfterLast()) {
						tbl = result.addTable(c.getString(0));

						// for the table just added to dictionary grab the
						// fields
						// directly from sqlite database
						if (null != tbl) {
							c1 = sqlDb.rawQuery(
									"select sql from sqlite_master where type=\"table\" and name=\""
											+ tbl.tblName + "\";", null);
							c1.moveToFirst();
							if (!c1.isAfterLast()) {
								String sql = c1.getString(0).toLowerCase()
										.replace("  ", " ");

								// we got the raw definition of sqlite table,
								// remove
								// all those parts that are not important and
								// keep
								// only the "field : type" comma-delimited pairs
								sql = sql
										.replace("create table " + tbl.tblName,
												"").replace("(", "")
										.replace(")", "");

								sql = sql.replace("  ", " ").trim();

								// split the comma-delimited fields into rows;
								// on
								// the first column we'll have the column name,
								// in
								// the second column we'll have the column type
								String[] fieldDefs = sql.split(",");
								for (int i = 0; i < fieldDefs.length; i++) {
									// add the sqlite column name to its table
									String[] coldef = fieldDefs[i].trim()
											.split(" ");
									tbl.addField(coldef[0]);
								}
							}
							try {
							} finally {
								c1.close();
							}
						}
						c.moveToNext();
					}
				} finally {
					c.close();
				}
			} catch (Exception e) {
				result = null;
				LogError(e.getMessage(), true);
			}
		}
		return result;
	}

	/**
	 * Read the application shared preferences and return a list with those
	 * dictionary found as installed
	 * 
	 * @return a List of DictionaryFile containing the installed dictionaries
	 */
	private List<DictionaryFile> getInstalledDictionaries() {
		return appPref.items;
	}

	private String getlistFromWordList(ArrayList<LexiconAdapterItem> wordList) {
		String result = "";
		for (int i = 0; i < wordList.size(); i++)
			result += (result.length() > 0 ? "," : "")
					+ String.valueOf(wordList.get(i).word_id);
		return result;
	}

	/**
	 * Generates a string array of elements as aa,ab,...az,ba,bb,...,zz
	 * 
	 * @param splits
	 *            how many elements should calculate and return
	 * @return the string array containing those elements
	 */
	private String[] getSplitPartsArray(int splits) {
		String[] result = new String[splits];
		final int mod = 'z' - 'a' + 1;
		final int a = 'a';
		char l1, l2;
		for (char i = 'a'; i < splits + 'a'; i++) {

			l2 = (char) (a + ((i - a) % mod));
			l1 = (char) (((i - l2) / mod) + 'a');
			result[i - 'a'] = "" + l1 + l2;
		}
		return result;
	}

	/**
	 * Load the Bitmap object into Tesseract OCR engine and returns a string
	 * representation of the text within bitmap
	 * 
	 * @param bitmap
	 *            the Bitmap that contains the text which has to be recognized
	 *            as characters
	 * @return the string recognized by OCR engine from the supplied bitmap
	 */
	private String getTextFromOCRBitmap(Bitmap bitmap) {
		String lang = currentDictionary == null
				|| currentDictionary.source_language.length() == 0 ? "eng"
				: currentDictionary.source_language;

		TessBaseAPI baseApi = new TessBaseAPI();
		baseApi.init(Utils.getDbDirectory(this), lang);
		baseApi.setImage(bitmap);

		String recognizedText = baseApi.getUTF8Text();
		baseApi.end();
		return recognizedText;
	}

	/**
	 * Retrieve from the Internet a List of all the available dictionaries.
	 * 
	 * @return a list of those dictionaries
	 * @throws IOException
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 */
	private List<DictionaryFile> getWebAvailDictionaryList()
			throws ParserConfigurationException, SAXException {
		List<DictionaryFile> result = null;
		final File tmpFile = new File(Environment.getExternalStorageDirectory()
				+ "/download/dict.xml");
		final ArrayList<String> parts = new ArrayList<String>();
		parts.add("dict.xml");
		try {
			if (Utils.DownloadOnSDcard(this, appPref.dwlBufferSize * 1024,
					DOWNLOAD_ROOT, parts, tmpFile.getAbsolutePath(), false,
					null, null))
				result = traverse(tmpFile);
		} catch (IOException e) {
		}
		tmpFile.delete();
		return result;
	}

	/**
	 * Read from the sqlite database all the tables for a specified word (based
	 * on its id) and compiles a Word structure that will contain everything
	 * found about it
	 * 
	 * @param word_id
	 *            the id of the word to be fetched
	 * @param options
	 *            PrintOptions based on which the information retrieved and
	 *            prepared will be filtered/limited
	 * @param onProgress
	 *            an event that might be used to show a progress of the Word
	 *            fetching
	 * @return a Word object whose values are fetched from sqlite database
	 *         containing 'everything' related to it
	 */
	synchronized private Word getWord(int word_id, Set<PrintOptions> options,
			IProgressUpdater onProgress) {
		long start = 0;// System.currentTimeMillis();
		final Resources res = getResources();
		int pos = 1;
		Word result = null;
		if (word_id > 0 && currentDb != null && currentDb.isOpen()) {
			Cursor c;
			result = new Word();
			String[] args = new String[] { String.valueOf(word_id) };

			if (onProgress != null)
				onProgress.setMaxPosition(14);

			// /////////////////////////////////////////////////////////////
			// read word attributes base on one-to-one table relationship
			// /////////////////////////////////////////////////////////////
			String sql = currentDictionary.getWordSqlQuery();
			c = currentDb.rawQuery(sql, args);

			c.moveToFirst();
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step1), start);
			try {
				if (!c.isAfterLast()) {
					// columns are indexed based on the above sqlite query

					result.word_id = c.getInt(0);
					result.value = c.getString(1);
					try {
						result.wordClass = Classes.valueOf(c.getString(2));

					} catch (Exception e) {
						result.wordClass = Classes.UNKNOWN_CLASS;
					}

					try {
						result.language = Languages.valueOf(c.getString(3));

					} catch (Exception e) {
						result.language = Languages.UNKNOWN_LANG;
					}

					result.comment = c.getString(4);

					// DEFINITION
					start = System.currentTimeMillis();
					if (options.contains(PrintOptions.DEFINITION)) {

						WordDefinition definition = new WordDefinition(
								c.getString(5), c.getInt(13));

						List<WordTranslation> l = getWordTranslation(word_id,
								"definition_id", c.getString(13));
						definition.translation = l;
						result.definition = definition;
					}

					// ////////////////////////////////////////////////////////////////
					if (onProgress != null)
						onProgress.setProgress(pos++,
								res.getString(R.string.main_wordprog_step2),
								start);
					// ////////////////////////////////////////////////////////////////

					// EXPLANATION
					start = System.currentTimeMillis();
					if (options.contains(PrintOptions.EXPLANATION)) {
						WordExplanation explanation = new WordExplanation(
								c.getString(6), c.getInt(13));
						explanation.translation = getWordTranslation(word_id,
								"explanation_id", c.getString(14));
						result.explanation = explanation;
					}

					// ////////////////////////////////////////////////////////////////
					if (onProgress != null)
						onProgress.setProgress(pos++,
								res.getString(R.string.main_wordprog_step3),
								start);
					// ////////////////////////////////////////////////////////////////

					start = System.currentTimeMillis();
					// GRAMMAR
					if (options.contains(PrintOptions.GRAMMAR))
						result.grammar = new WordGrammar(c.getString(7),
								c.getInt(15));

					// PHONETIC
					if (options.contains(PrintOptions.PHONETIC))
						result.phonetic = new WordPhonetic(c.getString(8),
								c.getString(9), c.getInt(16));

					// USE
					if (options.contains(PrintOptions.USE))
						result.use = new WordUse(c.getString(10), c.getInt(17));

					// VARIANT
					if (options.contains(PrintOptions.VARIANT))
						result.variant = new WordVariant(c.getString(11),
								c.getString(12), c.getInt(18));

					// ////////////////// FINITA LA COMEDIA !!!
					// ///////////////////////
					if (onProgress != null)
						onProgress.setProgress(pos++,
								res.getString(R.string.main_wordprog_step4),
								start);
					// ////////////////////////////////////////////////////////////////
				}

			} finally {
				c.close();
			}

			// /////////////////////////////////////////////////////////////
			// read word attributes base on one-to-many table relationship
			// /////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("compound")
					&& options.contains(PrintOptions.COMPOUND)) {
				// COMPOUND
				c = currentDb
						.rawQuery(
								"select value,compound_id from compound where word_id=?;",
								args);
				c.moveToFirst();
				try {
					List<WordCompound> compound = new ArrayList<WordCompound>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						WordCompound e = new WordCompound(c.getString(0),
								c.getInt(1));
						e.translation = getWordTranslation(word_id,
								"compound_id", c.getString(1));
						compound.add(e);
						c.moveToNext();
					}
					result.compound = compound;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step5), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("derivation")
					&& options.contains(PrintOptions.DERIVATION)) {
				// DERIVATION
				c = currentDb
						.rawQuery(
								"select value,derivation_id from derivation where word_id=?;",
								args);
				c.moveToFirst();
				try {
					List<WordDerivation> derivation = new ArrayList<WordDerivation>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						WordDerivation d = new WordDerivation(c.getString(0),
								c.getInt(1));
						d.translation = getWordTranslation(word_id,
								"derivation_id", c.getString(1));
						derivation.add(d);
						c.moveToNext();
					}
					result.derivation = derivation;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step6), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("example")
					&& options.contains(PrintOptions.EXAMPLE)) {
				// EXAMPLE
				c = currentDb
						.rawQuery(
								"select value,example_id from example where word_id=?;",
								args);
				c.moveToFirst();
				try {
					List<WordExample> example = new ArrayList<WordExample>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						WordExample e = new WordExample(c.getString(0),
								c.getInt(1));
						e.translation = getWordTranslation(word_id,
								"example_id", c.getString(1));
						example.add(e);
						c.moveToNext();
					}
					result.example = example;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step7), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("idiom")
					&& options.contains(PrintOptions.IDIOM)) {
				// IDIOM
				c = currentDb.rawQuery(
						"select value,idiom_id from idiom where word_id=?;",
						args);
				c.moveToFirst();
				try {
					List<WordIdiom> idiom = new ArrayList<WordIdiom>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						WordIdiom i = new WordIdiom(c.getString(0), c.getInt(1));
						i.translation = getWordTranslation(word_id,
								"example_id", c.getString(1));
						idiom.add(i);
						c.moveToNext();
					}
					result.idiom = idiom;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step8), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("inflection")
					&& options.contains(PrintOptions.INFLECTION)) {
				// INFLECTION
				c = currentDb
						.rawQuery(
								"select value,inflection_id from inflection where word_id=?;",
								args);
				c.moveToFirst();
				try {
					List<WordInflection> inflection = new ArrayList<WordInflection>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						inflection.add(new WordInflection(c.getString(0), c
								.getInt(1)));
						c.moveToNext();
					}
					result.inflection = inflection;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step9), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("related")
					&& options.contains(PrintOptions.RELATED)) {
				// RELATED
				c = currentDb
						.rawQuery(
								"select value,type,related_id from related where word_id=?;",
								args);
				c.moveToFirst();
				try {
					List<WordRelated> related = new ArrayList<WordRelated>();
					while (!c.isAfterLast()) {
						WordRelated r = new WordRelated(c.getString(0),
								c.getString(1), c.getInt(2));
						r.translation = getWordTranslation(word_id,
								"related_id", c.getString(2));
						// columns are indexed based on the above sqlite query
						related.add(r);
						c.moveToNext();
					}
					result.related = related;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step10), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("see")
					&& options.contains(PrintOptions.SEE)) {
				// SEE
				c = currentDb.rawQuery(
						"select value,type,see_id from see where word_id=?;",
						args);
				c.moveToFirst();
				try {
					List<WordSee> see = new ArrayList<WordSee>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						see.add(new WordSee(c.getString(0), c.getString(1), c
								.getInt(2)));
						c.moveToNext();
					}
					result.see = see;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step11), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("synonym")
					&& options.contains(PrintOptions.SYNONYM)) {
				// SYNONYM
				c = currentDb
						.rawQuery(
								"select value,level,synonym_id from synonym where word_id=?;",
								args);
				c.moveToFirst();
				try {
					byte minor, major;
					List<WordSynonym> synonym = new ArrayList<WordSynonym>();
					while (!c.isAfterLast()) {
						minor = 0;
						major = 0;
						// columns are indexed based on the above sqlite query
						if (null != c.getString(1)) {
							if (c.getString(1).length() > 0)
								major = Byte.parseByte(c.getString(1)
										.substring(0, 1));
							if (c.getString(1).length() > 2)
								minor = Byte.parseByte(c.getString(1)
										.substring(2, 3));
						}
						synonym.add(new WordSynonym(c.getString(0), minor,
								major, c.getInt(2)));
						c.moveToNext();
					}
					result.synonym = synonym;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step12), start);
			// ////////////////////////////////////////////////////////////////
			start = System.currentTimeMillis();

			if (currentDictionary.tableExists("url")
					&& options.contains(PrintOptions.PICTURE)) {
				// URL
				c = currentDb.rawQuery(
						"select value,type,url_id from url where word_id=?;",
						args);
				c.moveToFirst();
				try {
					List<WordURL> picture = new ArrayList<WordURL>();
					while (!c.isAfterLast()) {
						// columns are indexed based on the above sqlite query
						picture.add(new WordURL(c.getString(0), c.getString(1),
								c.getInt(2)));
						c.moveToNext();
					}
					result.picture = picture;

				} finally {
					c.close();
				}
			}

			// ////////////////////////////////////////////////////////////////
			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step13), start);
			// ////////////////////////////////////////////////////////////////

			start = System.currentTimeMillis();
			result.translation = getWordTranslation(word_id, null, null);

			if (onProgress != null)
				onProgress.setProgress(pos++,
						res.getString(R.string.main_wordprog_step14), start);
		}
		return result;
	}

	/**
	 * Fetch the translation for a specified word from the sqlite database
	 * 
	 * @param wordId
	 *            the Id of the word whose translation is fetched from sqlite
	 *            database
	 * @param parent_key
	 *            if the translation is for the word itself then specify here
	 *            null otherwise the foreign key used to identify the identity
	 *            for which the translation should be fetched (ex: idiom_id,
	 *            definition_id, etc)
	 * @param parent_value
	 *            if the translation is for the word itself then specify here
	 *            null otherwise the value of the key used to identify the
	 *            entity the translation is belonging to
	 * @return an List containing founded translations of the specified word
	 *         (and of its parent item, if specified)
	 */
	private List<WordTranslation> getWordTranslation(int wordId,
			String parent_key, String parent_value) {
		List<WordTranslation> result = new ArrayList<WordTranslation>();
		SqliteTableDef translation = currentDictionary.getTable("translation");

		String sqlQuery = "select value,comment,translation_id from translation where word_id=?";
		if (parent_key == null || parent_key.length() == 0
				|| !translation.fieldExists(parent_key)) {
			sqlQuery +=

			/* only word translation */
			(translation.fieldExists("definition_id") ? " and definition_id is null"
					: "")
					+ (translation.fieldExists("example_id") ? " and example_id is null"
							: "")
					+ (translation.fieldExists("idiom_id") ? " and idiom_id is null"
							: "")
					+ (translation.fieldExists("related_id") ? " and related_id is null"
							: "")
					+ (translation.fieldExists("explanation_id") ? " and explanation_id is null"
							: "")
					+ (translation.fieldExists("compound_id") ? " and compound_id is null"
							: "")
					+ (translation.fieldExists("derivation_id") ? " and derivation_id is null "
							: "");
			/* word's detail translation */
		} else
			sqlQuery += " and "
					+ parent_key
					+ (parent_value == null ? " is null" : "=\"" + parent_value
							+ "\"");

		sqlQuery += " and length(value)>0;";

		if (currentDictionary.tableExists("translation")) {
			Cursor c = currentDb.rawQuery(sqlQuery,
					new String[] { String.valueOf(wordId) });
			c.moveToFirst();
			try {
				while (!c.isAfterLast()) {
					// columns are indexed based on the above sqlite query
					result.add(new WordTranslation(c.getString(0), c
							.getString(1), c.getInt(2)));
					c.moveToNext();
				}

			} finally {
				c.close();
			}
		}
		return result;
	}

	/**
	 * Hide the android software keyboard
	 */
	private void hideSoftKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edtSearch.getWindowToken(), 0);
	}

	/**
	 * This method should be used to connect to the current dictionary. If no
	 * dictionary available it will suggest to install a new one.
	 */
	private void initApp() {
		// word list
		listAdapter = new ArrayList<LexiconAdapterItem>();
		dictListAdapter = new DictionaryListAdapter(this, listAdapter);
		dictListAdapter.setShowTranslation(appPref.showTranslation);
		setListAdapter(dictListAdapter);

		if (appPref.items.size() > 0) {
			try {
				currentDb = openDb(currentDb,
						appPref.items.get(appPref.getDictIndex()).fileName);
			} catch (Exception e) {
				LogError(e.getMessage(), true);
				currentDb = null;
			}
		}

		boolean unknownDict = null == currentDb;
		if (null != currentDb)
			unknownDict = unknownDict || !loadDictionary();

		if (unknownDict || null == currentDb) {
			// delete the damadge dictionary from our preferences; it will
			// became available for installation
			if (appPref.getDictIndex() >= 0) {
				appPref.delDictionary(appPref.items.get(appPref.getDictIndex()).fileName);
				if (appPref.items.size() > 0)
					appPref.setDictionaryIndex(0);
				else
					appPref.setDictionaryIndex(-1);
			}

			btnSearch.setVisibility(View.GONE);
			edtSearch.setVisibility(View.GONE);
			txtSearchStatus.setVisibility(View.GONE);
			btnMask.setVisibility(View.GONE);
			btnClearAll.setVisibility(View.GONE);

			new AlertDialog.Builder(this)
					.setMessage(getString(R.string.db_not_installed))
					.setCancelable(true)
					.setTitle(getString(R.string.dialog_error))
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(getString(R.string.dialog_yes),
							new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									installDictionaryDialog(dialog, true);
								}
							})
					.setNegativeButton(getString(R.string.dialog_no),
							new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Main.this.finish();
								}
							}).show();
		} else {
			btnSearch.setVisibility(View.VISIBLE);
			edtSearch.setVisibility(View.VISIBLE);
			txtSearchStatus.setVisibility(View.VISIBLE);
			btnMask.setVisibility(View.VISIBLE);
			btnClearAll.setVisibility(View.VISIBLE);
		}
		if (appPref.clearByShaking && null == shaker)
			shaker = new Shaker(this, 1.25d, 1000, this);
		else
			destroyShaker();
	}

	/**
	 * Provides a dialog which suggests the available dictionaries to be
	 * installed. If anything is chosen then they will be installed.
	 * 
	 * @param terminateOnCancel
	 *            if true then finish the application when user cancel this
	 *            dialog
	 */
	private void installDictionaryDialog(DialogInterface dialog,
			final boolean terminateOnCancel) {
		if (null != dialog)
			dialog.dismiss();
		final ArrayList<String> items = new ArrayList<String>();
		final Resources res = getResources();
		List<DictionaryFile> availDictionary;
		try {
			availDictionary = getWebAvailDictionaryList();
			List<DictionaryFile> instDictionary = getInstalledDictionaries();
			final List<DictionaryFile> chkDictionary = new ArrayList<DictionaryFile>();

			for (int i = 0; i < availDictionary.size(); i++) {
				boolean found = false;
				for (int j = 0; !found && j < instDictionary.size(); j++)
					if (instDictionary.get(j).fileName
							.compareToIgnoreCase(availDictionary.get(i).fileName) == 0)
						found = true;
				// we display only available dictionary not installed
				if (!found) {
					items.add(availDictionary.get(i).name);
					chkDictionary.add(availDictionary.get(i));
				}
			}
			final DictionaryDialogListAdapter dictDlgListAdapter = new DictionaryDialogListAdapter(
					this, (ArrayList<DictionaryFile>) (chkDictionary), true);

			AlertDialog alert = null;
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			View view = LayoutInflater.from(this).inflate(
					R.layout.dictionary_dialog,
					(LinearLayout) findViewById(R.id.dict_dialog_root));
			builder.setView(view);
			builder.setTitle(res.getString(R.string.main_dict_availdict));
			builder.setCancelable(true);
			builder.setAdapter(dictDlgListAdapter, null);
			builder.setPositiveButton(res.getString(R.string.btn_continue),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							vMaxDownload = 0; // reset the overall downloaded
												// size
							for (int i = 0; i < dictDlgListAdapter.getCount(); i++)
								if (dictDlgListAdapter.getChecked(i))
									updateDictionary(chkDictionary.get(i));
							dialog.dismiss();
						}
					});
			builder.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					if (terminateOnCancel) {
						dialog.dismiss();
						Main.this.finish();
					}
				}
			});
			alert = builder.create();
			alert.show();
		} catch (ParserConfigurationException e) {
			Utils.lexToast1(this, e.getMessage(),
					android.R.drawable.ic_dialog_alert);
		} catch (SAXException e) {
			Utils.lexToast1(this, e.getMessage(),
					android.R.drawable.ic_dialog_alert);
		}

	}

	/**
	 * * Download and install a new dictionary available on the Internet
	 * 
	 * It is expected that the dictionary is splitted in more than one parts
	 * with Linux command: split -b <split size> <original dictionary> <splitted
	 * dictionary> + "_" + split part (such aa, ab, ac, ...)
	 * 
	 * @param dictFile
	 *            a dictionary file to be installed from web
	 * @param outputPath
	 *            the full path where to install the file
	 * @return true if dictionary installed successfully
	 * @throws IOException
	 */
	private boolean installDictionaryFromWeb(DictionaryFile dictFile,
			String outputPath) throws IOException {

		boolean result = false;
		final Resources res = getResources();
		if (!Utils.isOnline(this)) {
			new AlertDialog.Builder(this)
					.setMessage(res.getString(R.string.main_dict_nointernet))
					.setTitle(res.getString(R.string.title_connerror))
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(res.getString(R.string.btn_terminate),
							new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Main.this.finish();
								}
							}).show();
			return false;
		}

		final ArrayList<String> parts = new ArrayList<String>();
		// final String dbFullPath = getDbDirectory();

		if (outputPath != null) {

			if (dictFile.fileSplitCount > 1) {
				String[] splits = getSplitPartsArray(dictFile.fileSplitCount);
				for (char i = 0; i < splits.length; i++)
					parts.add(dictFile.fileName + "_" + splits[i]);
			} else
				parts.add(dictFile.fileName);

			SQLiteDatabase tmp = null;
			Dictionary dict;
			try {
				try {
					tmp = openDb(tmp, dictFile.fileName);// is the file ok?
				} catch (Exception e) {
					tmp = null;// no, it isn't
				}
				dict = null;
				if (null != tmp && tmp.isOpen())// its ok but is right?
					dict = getDictionaryOnCurrentDb(tmp);
				if (null != dict)
					tmp.close();
				// asynkDownloadProgress = new
				// AsyncTaskDownloader(Main.this,dictFile,
				// dictFile.name, dictFile.root_url, parts, outputPath
				// + dictFile.fileName, vMaxDownload,
				// dictFile.archive, dictFile.type, dictFile.size,
				// dictFile.itemCount);
				asynkDownloadProgress = new AsyncTaskDownloader(Main.this,
						dictFile, parts, outputPath, vMaxDownload);
				asynkDownloadProgress.execute();
			} catch (Exception e) {
				LogError(e.getMessage());
				// if this is a download error then make sure we delete the last
				// downloaded part
				try {
					new File(outputPath + dictFile.fileName).delete();
				} catch (Exception e2) {
					// we should catch these errors to, right?
				}
			}
			result = true;
		}

		return result;
	}

	private boolean installOCRLibDialog(final String lang) {

		final Resources res = getResources();
		AlertDialog alert = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(res.getString(R.string.tesslib_not_found));
		builder.setMessage(res.getString(R.string.tesslib_not_installed));
		builder.setCancelable(true);
		builder.setNegativeButton(res.getString(R.string.btn_close), null);
		builder.setPositiveButton(res.getString(R.string.btn_continue),
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						final ArrayList<String> parts = new ArrayList<String>(
								Arrays.asList(lang + "_ocrdata"));

						vMaxDownload = 0; // reset the overall downloaded size

						if (!isTessDataInstalled(lang)) {
							final String tessdataPath = Utils
									.getTessdataPath(Main.this);
							new File(tessdataPath).mkdirs();

							final String fName = lang + ".traineddata";
							asynkDownloadProgress = new AsyncTaskDownloader(
									Main.this, new DictionaryFile(0, fName,
											fName, 1, DOWNLOAD_ROOT,
											DictionaryType.RAW, true, -1, -1),
									parts, tessdataPath, vMaxDownload);
							asynkDownloadProgress.execute();
						}
						dialog.dismiss();
					}
				});
		builder.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
			}
		});
		alert = builder.create();
		alert.show();

		return true;
	}

	private boolean isOCRLibInstalled() {

		boolean result = true;
		final String[] libs = { "libjpeg.so", "liblept.so", "libtess.so" };
		final String path = Environment.getDataDirectory().getAbsolutePath()
				+ "/data/" + getPackageName() + "/lib/";

		for (String lib : libs)
			if (!(new File(path + lib).exists())) {
				result = false;
				break;
			}

		return result;
	}

	private boolean isTessDataInstalled(String lang) {
		return (new File(Utils.getTessdataPath(this)
				+ (lang.length() > 0 ? lang : "eng") + ".traineddata"))
				.exists();
	}

	private boolean isTesseractInstalled(String lang) {
		boolean result = isTessDataInstalled(lang);
		if (!isOCRLibInstalled()) {
			final Resources res = getResources();
			AlertDialog alert = null;
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(res.getString(R.string.tessdata_not_found));
			builder.setMessage(res.getString(R.string.tessdata_not_installed));
			builder.setCancelable(true);
			builder.setPositiveButton(res.getString(R.string.btn_continue),
					null);
			builder.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					dialog.dismiss();
				}
			});
			alert = builder.create();
			alert.show();
		}
		return result;
	}

	/**
	 * Read the dictionary from the current sqlite connection and sets the
	 * currentDictionary to that
	 * 
	 * @return true if a valid dictionary found and loaded
	 */
	private boolean loadDictionary() {
		currentDictionary = getDictionaryOnCurrentDb(currentDb);
		if (null != currentDictionary)
			searchWord(listAdapter, (edtSearch.getText().toString()), true);
		return null != currentDictionary;
	}

	/**
	 * Display a generic error message on screen
	 * 
	 * @param errMsg
	 *            the message to be shown
	 */
	private void LogError(String errMsg) {
		final Resources res = getResources();
		new AlertDialog.Builder(Main.this).setMessage(errMsg)
				.setCancelable(false)
				.setPositiveButton(res.getString(R.string.btn_close), null)
				.setTitle(res.getString(R.string.dialog_error)).show();
	}

	/**
	 * Display a generic error message (on screen or in file, based on the
	 * silent argument)
	 * 
	 * @param errMsg
	 *            the message to be shown
	 * @param silent
	 *            if true then the error will be written to the application log
	 */
	private void LogError(String errMsg, boolean silent) {
		if (!silent)
			LogError(errMsg);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;

		if (requestCode == 0) {
			switch (resultCode) {
			case -1:
				doCrop();
				break;

			default:
				break;
			}
			return;
		}

		switch (requestCode) {
		case PICK_FROM_CAMERA:
			doCrop();

			break;

		case PICK_FROM_FILE:
			mImageCaptureUri = data.getData();

			doCrop();

			break;

		case CROP_FROM_CAMERA:
			Bundle extras = data.getExtras();

			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				try {
					FileOutputStream out = new FileOutputStream(
							Utils.getJPEGFile(this, "/ocr.jpg"));
					photo.compress(Bitmap.CompressFormat.JPEG, 90, out);
					onPhotoTaken();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			break;
		}
	}

	/**
	 * This should be called by AsyncTaskDetailLoader when it is done
	 */
	private void onAsyncTaskDetailLoaderDone(Object... params) {
		if (isDetailProgressDlgVisible) {
			dismissDialog(LOADING_DIALOG);
			isDetailProgressDlgVisible = false;
			if (params.length > 0 && null != params[0]
					&& appPref.createStatistics == true)
				updateStatistics((Integer) params[0]);
		}
		asyncWordFetchProgress = null;
	}

	/**
	 * Method called when a download async task is done
	 * 
	 * @param dictFile
	 *            the dictionary file object that has been downloaded
	 * @param outputFile
	 *            the full path of the downloaded file
	 * @param dictParts
	 *            the number of parts the downloaded file was splitted
	 * @param e
	 *            the exception that occurred during the download
	 */
	private void onAsyncTaskDownloaderDone(DictionaryFile dictFile,
			String outputFile, int dictParts, Exception e) {
		if (null == e && null != dictFile) {
			// we get the file name of the db from its path
			File f = new File(outputFile);
			if (dictFile.archive || f.exists()) {
				SQLiteDatabase tmp = null;
				try {
					// try to open the db
					if (!dictFile.archive)
						tmp = openDb(tmp, f.getName());
					if (dictFile.archive || null != tmp) {
						// get the db's dictionary
						Dictionary tmpDict = null;
						if (!dictFile.archive)
							tmpDict = getDictionaryOnCurrentDb(tmp);

						// if everything is valid then add the dictionary to
						// application preferences
						if (dictFile.type == DictionaryType.MP3
								|| null != tmpDict) {
							appPref.addDictionary(dictFile.name, f.getName(),
									dictParts, dictFile.root_url,
									dictFile.type, dictFile.archive,
									dictFile.size, dictFile.itemCount);
							appPref.refresh();
							if (null != tmp)
								tmp.close();
						}
					}
				} catch (IOException e1) {
					e = e1;
				}
			}
		}

		if (downloadDlg.getThreadCount() > 0)
			downloadDlg.decrementThreads();

		if (downloadDlg.getThreadCount() < 1 && isDownloadProgressDlgVisible) {
			dismissDialog(DOWNLOAD_DIALOG);
			isDownloadProgressDlgVisible = false;
			if (null == e)
				initApp();
			else
				LogError(e.getMessage());
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		appPref = new AppPreferences(this);

		currentDictionary = null;

		txtSearchStatus = (TextView) findViewById(R.id.txtStatus);

		edtSearch = (EditText) findViewById(R.id.edtSearch);
		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if ((actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_DOWN)
						|| (actionId == EditorInfo.IME_ACTION_DONE))
					if (!appPref.searchInPlace)
						searchWord(listAdapter, edtSearch.getText().toString(),
								true);
					else
						hideSoftKeyboard();

				return true;
			}
		});

		edtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				if (appPref.searchInPlace)
					searchWord(listAdapter, (edtSearch.getText() == null ? null
							: edtSearch.getText().toString()), true);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}
		});

		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!appPref.searchInPlace)
					searchWord(listAdapter, edtSearch.getText().toString(),
							true);
			}
		});

		getListView().setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				LexiconAdapterItem selectedItem = (LexiconAdapterItem) ((DictionaryListAdapter) arg0
						.getAdapter()).getItem(arg2);
				if (selectedItem != null) {
					final Intent detail = new Intent(Main.this, Detail.class);
					asyncWordFetchProgress = new AsyncTaskDetailLoader(detail,
							selectedItem.word_id, appPref.detailPrintOption,
							Main.this);
					asyncWordFetchProgress.execute();
				}
			}
		});

		// button %
		btnMask = ((Button) findViewById(R.id.btnMask));
		btnMask.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (edtSearch.getText().length() > 0
						&& edtSearch.getText().charAt(0) == '%')
					edtSearch.getText().delete(0, 1);
				else
					edtSearch.getText().insert(0, "%");
			}

		});

		// button-icon "clear text"
		btnClearAll = ((Button) findViewById(R.id.btnClearAll));
		btnClearAll.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				edtSearch.setText("");
				showSoftKeyboard();
			}

		});

		if (_ENABLE_TEST_CASE) {
			Button btnTestCase = new Button(this, null,
					android.R.attr.buttonStyleSmall);
			LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(
					android.view.ViewGroup.LayoutParams.FILL_PARENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
			lp1.gravity = android.view.Gravity.CENTER;

			btnTestCase.setLayoutParams(new LayoutParams(lp1));
			btnTestCase.setText("Run all test cases...");
			btnTestCase.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					testCase(0);
				}
			});

			LinearLayout myLayout = (LinearLayout) findViewById(R.id.layTop);
			myLayout.addView(btnTestCase);
		}

		// initApp(); -- is already called onResume() which starts allwayse

		if (savedInstanceState != null) {
			searchPattern = savedInstanceState.getString("pattern");
			edtSearch.setText(searchPattern);
		} else
			searchPattern = "";

		Object retained = getLastNonConfigurationInstance();
		if (retained instanceof AsyncTaskDetailLoader) {
			asyncWordFetchProgress = (AsyncTaskDetailLoader) retained;
			asyncWordFetchProgress.setActivity(this);
			if (asyncWordFetchProgress.completed && null != wordDetailDlg)
				wordDetailDlg.dismiss();
		} else if (retained instanceof AsyncTaskDownloader) {
			asynkDownloadProgress = (AsyncTaskDownloader) retained;
			asynkDownloadProgress.setActivity(this);
			if (asynkDownloadProgress.completed && null != downloadDlg)
				downloadDlg.dismiss();
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		Dialog result = null;
		switch (id) {
		case LOADING_DIALOG:
			wordDetailDlg = new ProgressDialog(this);
			wordDetailDlg.setCancelable(false);
			wordDetailDlg.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			isDetailProgressDlgVisible = true;
			result = wordDetailDlg;
			break;
		case DOWNLOAD_DIALOG:
			downloadDlg = new DownloadProgressDialog(this);
			downloadDlg.setStyle(ProgressDialog.STYLE_HORIZONTAL);
			downloadDlg.setTitle(getResources().getString(
					R.string.main_dwl_wait));
			downloadDlg.setVisibilityCurrent(View.GONE);
			downloadDlg.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					asynkDownloadProgress.cancel(false);
				}
			});
			isDownloadProgressDlgVisible = true;
			result = downloadDlg;
			break;
		default:
			break;
		}

		return result;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		destroyShaker();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_CAMERA) {
			startCameraActivity();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		final Resources res = getResources();
		switch (item.getItemId()) {
		case R.id.item_license:
			Intent aboutActivity = new Intent(this, License.class);
			aboutActivity.putExtra("title", getString(R.string.app_title) + "-"
					+ getString(R.string.menu_license).toLowerCase());
			startActivity(aboutActivity);
			break;
		case R.id.item_sort_asc:
		case R.id.item_sort_desc:
			appPref.listSortedAscendent = !appPref.listSortedAscendent;
			searchWord(listAdapter, edtSearch.getText().toString(), true);
			break;
		case R.id.item_sort_settings:
			Intent settingsActivity = new Intent(this, Preferences.class);
			settingsActivity.putExtra("title", getString(R.string.app_title)
					+ "-" + getString(R.string.menu_settings).toLowerCase());
			startActivity(settingsActivity);
			break;
		case R.id.item_dictionary:
			final CharSequence[] items = appPref
					.getDictionaryNames(DictionaryType.WORD);
			AlertDialog alert = null;
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			DictionaryDialogListAdapter dictDlgListAdapter = new DictionaryDialogListAdapter(
					this, new ArrayList<DictionaryFile>(appPref.items), false);
			dictDlgListAdapter.setCheckedIndex(appPref.getDictIndex(), true);
			View view = LayoutInflater.from(this).inflate(
					R.layout.dictionary_dialog,
					(LinearLayout) findViewById(R.id.dict_dialog_root));
			builder.setView(view);
			builder.setTitle(getString(R.string.prompt_dict));
			builder.setCancelable(true);
			if (Utils.isOnline(this))
				builder.setPositiveButton(res.getString(R.string.btn_install),
						new OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								installDictionaryDialog(dialog, false);
							}
						});
			builder.setNeutralButton(res.getString(R.string.btn_uninstall),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							uninstallDictionaryDialog(dialog);
						}
					});
			builder.setNegativeButton(res.getString(R.string.btn_info),
					new OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							showDictionaryInfo(appPref.getDictIndex());
						}
					});

			dictDlgListAdapter.onClick = new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					if ((appPref.getDictIndex() != which || currentDb == null)
							&& which < items.length) {
						SQLiteDatabase tmp = null;
						try {
							tmp = openDb(tmp, appPref.items.get(which).fileName);
						} catch (IOException e) {
							LogError(e.getMessage(), true);
							tmp = null;
						}
						if (tmp != null) {
							if (currentDb != null && currentDb.isOpen()) {
								currentDb.close();
								currentDb = null;
							}
							currentDb = tmp;
							appPref.setDictionaryIndex(which);
							loadDictionary();
							dialog.dismiss();
						}
					} else
						dialog.dismiss();
				}
			};

			builder.setAdapter(dictDlgListAdapter, null);
			alert = builder.create();
			dictDlgListAdapter.setDialog(alert);
			alert.show();
			break;
		case R.id.item_update_app:
			updateApp();
			break;
		case R.id.item_update_dict:
			vMaxDownload = 0; // reset the overall downloaded size
			for (DictionaryFile dict : getInstalledDictionaries())
				updateDictionary(dict);
			break;
		case R.id.item_ocr:
			startCameraActivity();
			break;
		default:
			break;
		}

		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (!((null != asyncWordFetchProgress) && (asyncWordFetchProgress
				.getStatus() == Status.RUNNING)))
			if (currentDb != null && currentDb.isOpen()) {
				currentDb.close();
				currentDb = null;
			}
	}

	/**
	 * Method to be called right after the camera has taken a photo
	 * 
	 * The text recognized from the photo will become our word to search for it
	 * into the current dictionary
	 */
	private void onPhotoTaken() {
		Bitmap bitmap;
		try {
			bitmap = fixPhoto(Utils.getJPEGFile(this, "/ocr.jpg")
					.getAbsolutePath());
			String ocrText = getTextFromOCRBitmap(bitmap);

			Utils.getJPEGFile(this, "/ocr.jpg").delete();

			ocrText = ocrText.replaceAll("[^a-zA-Z0-9]+", " ").trim();
			edtSearch.setText(ocrText);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {
		super.onPrepareDialog(id, dialog);
		switch (id) {
		case LOADING_DIALOG:
			isDetailProgressDlgVisible = true;
			break;
		case DOWNLOAD_DIALOG:
			isDownloadProgressDlgVisible = true;
			break;
		default:
			break;
		}

	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.item_sort_asc)
				.setVisible(!appPref.listSortedAscendent)
				.setEnabled(foundWords > 1);
		menu.findItem(R.id.item_sort_desc)
				.setVisible(appPref.listSortedAscendent)
				.setEnabled(foundWords > 1);
		menu.findItem(R.id.item_update).setVisible(Utils.isOnline(this));
		return true;
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState != null) {
			searchPattern = savedInstanceState.getString("pattern");
			edtSearch.setText(searchPattern);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		appPref = new AppPreferences(this);
		Button btnSearch = (Button) findViewById(R.id.btnSearch);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		if (appPref.searchInPlace)
			lp.width = 0;
		btnSearch.setLayoutParams(lp);
		initApp();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if (null != asyncWordFetchProgress)
			asyncWordFetchProgress.setActivity(null);
		if (null != asynkDownloadProgress)
			asynkDownloadProgress.setActivity(null);

		// if (null != asyncWordFetchProgress || null != asynkDownloadProgress)
		// Utils.lexToast(Main.this, getString(R.string.app_config_changed));

		return null != asyncWordFetchProgress ? asyncWordFetchProgress
				: asynkDownloadProgress;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString("pattern", searchPattern);
	}

	/**
	 * Returns the sqlite connection to the specified dbName sqlite database
	 * 
	 * @return a reference to a SQLiteDatabase connection or null in case of
	 *         error
	 * @throws IOException
	 */
	private SQLiteDatabase openDb(SQLiteDatabase db, String dbName)
			throws IOException {
		if (db != null && db.isOpen()) {
			db.close();
			db = null;
		}
		SQLiteDatabase sqlDb = null;

		String dbFullPath = Utils.getDbDirectory(this) + dbName;

		if (dbFullPath != null) {
			if (new File(dbFullPath).exists()) {
				sqlDb = SQLiteDatabase
						.openDatabase(
								dbFullPath,
								null,
								appPref.createStatistics ? SQLiteDatabase.OPEN_READWRITE
										: SQLiteDatabase.OPEN_READONLY
												| SQLiteDatabase.NO_LOCALIZED_COLLATORS);
			} else
				throw (new IOException(String.format(
						getString(R.string.db_not_found), dbFullPath)));
		}
		return sqlDb;
	}

	/**
	 * Returns the matching words searched by pattern
	 * 
	 * This method is worst written than populateList1 but it's a little bit
	 * faster (~ 30%)
	 * 
	 * @param wordList
	 *            the array to be populated with the TOP n matched records
	 * @param pattern
	 *            string pattern used to query the word table
	 * @return true if everything worked well
	 */
	private boolean populateList(ArrayList<LexiconAdapterItem> wordList,
			String pattern) {
		int recCount = -1;
		boolean result = false;
		if (!appPref.showTranslation)
			return populateList2(wordList, pattern);
		else {
			if (null == wordList)
				return result;

			wordList.clear();

			// read from word table
			recCount = populateListBySql(
					wordList,
					"select word.word_id,word.value,translation.value from word "
							+ "left outer join translation on word.word_id=translation.word_id "
							+ "where word.word_id in (select word_id from word "
							+ (pattern != null && pattern.length() == 0 ? ""
									: "where value "
											+ (pattern == null ? "is null"
													: "like \"" + pattern
															+ "%\""))
							+ " limit "
							+ String.valueOf(appPref.searchTopResult)
							+ ") "
							+ (!appPref.listSortedAscendent ? "order by word.value desc "
									: "") + ";");

			// read from inflection table (upto searchTopResult records)
			if (appPref.searchDeep && pattern != null && pattern.length() > 0) {
				int recDiff = appPref.searchTopResult - recCount;
				if (recDiff > 0)
					recCount += populateListBySql(
							wordList,
							"select word.word_id,word.value,translation.value from word "
									+ "left outer join translation on word.word_id=translation.word_id "
									+ "where word.word_id in (select word_id from inflection "
									+ "where value like \""
									+ pattern
									+ "%\""
									+ " group by word_id limit "
									+ String.valueOf(recDiff)
									+ ") and word.word_id not in ("
									+ getlistFromWordList(wordList)
									+ ")"
									+ (!appPref.listSortedAscendent ? "order by word.value desc "
											: "") + ";");
			}
		}
		return recCount >= 0;
	}

	/**
	 * This method is different by populateList/populateList1 because does not
	 * return translation, but when no translation is need it then it's
	 * recommended to use this method because it's ~20% faster than populateList
	 * and 40% than populateList1
	 * 
	 * @param wordList
	 *            the array to be populated with the TOP n matched records
	 * @param pattern
	 *            string pattern used to query the word table
	 * @return true if everything worked well
	 */
	private boolean populateList2(ArrayList<LexiconAdapterItem> wordList,
			String pattern) {
		int recCount = -1;
		boolean result = false;

		if (null == wordList)
			return result;

		wordList.clear();

		// read from word table
		recCount = populateListBySql2(
				wordList,
				"select word_id,value from word "
						+ (pattern != null && pattern.length() == 0 ? ""
								: "where value "
										+ (pattern == null ? "is null"
												: "like \"" + pattern + "%\""))
						+ " limit "
						+ Integer.toString(appPref.searchTopResult)
						+ (!appPref.listSortedAscendent ? " order by value desc "
								: "") + ";");

		// read from inflection table (upto searchTopResult records)
		if (appPref.searchDeep && pattern != null && pattern.length() > 0) {
			int recDiff = appPref.searchTopResult - recCount;
			if (recDiff > 0)
				recCount += populateListBySql2(
						wordList,
						"select word_id,value from inflection "
								+ "where value like \""
								+ pattern
								+ "%\" and word_id not in ("
								+ getlistFromWordList(wordList)
								+ ")"
								+ " limit "
								+ Integer.toString(recDiff)
								+ (!appPref.listSortedAscendent ? " order by value desc "
										: "") + ";");
		}

		return recCount >= 0;
	}

	/**
	 * Executes the sqlStatement and populate the wordList with the words found.
	 * This method is used by {@link #populateList(ArrayList, String)}
	 * 
	 * @param wordList
	 *            the array populated with the TOP n matched records
	 * @param sqlStatement
	 *            the SQL statement based on which query the database
	 * @return the found record count or -1 on error
	 */
	private int populateListBySql(ArrayList<LexiconAdapterItem> wordList,
			String sqlStatement) {
		int result = -1;

		if (null == wordList)
			return result;

		try {
			if (currentDb != null && currentDb.isOpen()) {
				Cursor c = currentDb.rawQuery(sqlStatement, null);
				c.moveToFirst();
				try {
					int word_id, old_word_id = -1;
					StringBuffer buff = new StringBuffer();
					String value = "", transCol = "";
					while (!c.isAfterLast()) {
						word_id = c.getInt(0);
						if (word_id != old_word_id) {
							if (old_word_id > 0) {
								wordList.add(new LexiconAdapterItem(
										old_word_id, value, buff.toString()));
								buff.setLength(0);
							}
							value = c.getString(1);
							old_word_id = word_id;
						}
						transCol = c.getString(2);
						if (transCol != null && transCol.length() > 0)
							buff.append(transCol + ",");
						c.moveToNext();
					}
					if (old_word_id > 0)
						wordList.add(new LexiconAdapterItem(old_word_id, value,
								buff.toString()));
					result = c.getCount();
				} finally {
					c.close();
				}
			}
		} catch (Exception e) {
			LogError(e.getMessage(), true);
		}

		return result;
	}

	/**
	 * Executes the sqlStatement and populate the wordList with the words found.
	 * This method is used by {@link #populateList2(ArrayList, String)}
	 * 
	 * @param wordList
	 *            the array populated with the TOP n matched records
	 * @param sqlStatement
	 *            the SQL statement based on which query the database
	 * @return the found record count or -1 on error
	 */
	private int populateListBySql2(ArrayList<LexiconAdapterItem> wordList,
			String sqlStatement) {
		int result = -1;

		if (null == wordList)
			return result;

		try {
			if (currentDb != null && currentDb.isOpen()) {
				Cursor c = currentDb.rawQuery(sqlStatement, null);
				c.moveToFirst();
				try {
					while (!c.isAfterLast()) {
						wordList.add(new LexiconAdapterItem(c.getInt(0), c
								.getString(1), null));
						c.moveToNext();
					}
				} finally {
					result = c.getCount();
					c.close();
				}
			}
		} catch (Exception e) {
			LogError(e.getMessage(), true);
		}
		return result;
	}

	/**
	 * Search the specified word and populates the result list with the matching
	 * records
	 * 
	 * @param items
	 *            the array list to be populated with the found results
	 * @param sSearchOrd
	 *            string pattern by which it'll query the word table
	 * @param notifyUI
	 *            if true then update the UI controls
	 */
	private void searchWord(ArrayList<LexiconAdapterItem> items,
			String sSearchOrd, boolean notifyUI) {
		if (sSearchOrd == null)
			return;
		if (!appPref.searchInPlace)
			hideSoftKeyboard();

		long start = System.currentTimeMillis();

		sSearchOrd = sSearchOrd.trim();
		if (populateList(items, sSearchOrd)) {
			foundWords = items.size();
			dictListAdapter.setShowTranslation(appPref.showTranslation);
			if (notifyUI)
				dictListAdapter.notifyDataSetChanged();

			float end = (float) ((System.currentTimeMillis() - start) / 1000.0);

			if (notifyUI)
				txtSearchStatus.setText(String.format("%d / %d "
						+ getString(R.string.words_found) + " (~%.1f\")",
						items.size(), currentDictionary.wordCount, sSearchOrd,
						end));
		}
	}

	/**
	 * This method takes care of setting the download progress
	 * 
	 * @param increment
	 * @param max
	 */
	private void setDownloadProgress(int increment, int max) {
		if (downloadDlg.getMaxProgOverall() != max)
			downloadDlg.setMaxProgOverall(max);
		else
			downloadDlg.setIncrementCurrent(increment);
	}

	/**
	 * Set the status of the download dialog
	 * 
	 * @param status
	 *            the string that will be used to update dialog status
	 */
	private void setDownloadStatus(String status) {
		downloadDlg.setTitleOverall(status);
	}

	@Override
	public void shakingStarted() {
		edtSearch.setText("");
		showSoftKeyboard();
	}

	@Override
	public void shakingStopped() {
	}

	/**
	 * Display dictionary details
	 */
	protected void showDictionaryInfo(int index) {
		final Resources res = getResources();
		final DictionaryFile df = index > -1 ? appPref.items.get(index) : null;

		final String crlf = "\n";
		final String space = " ";
		AlertDialog dialog = new AlertDialog.Builder(this)
				.setMessage("")
				.setCancelable(true)
				.setNegativeButton(res.getString(R.string.btn_close),
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						})
				.setMessage(
						res.getString(R.string.main_dict_infoname)
								+ space
								+ (null != df ? df.name
										: currentDictionary.name)
								+ crlf
								+ res.getString(R.string.main_dict_infover)
								+ space
								+ currentDictionary.version
								+ crlf
								+ res.getString(R.string.main_dict_infocreate)
								+ space
								+ currentDictionary.created
								+ crlf
								+ res.getString(R.string.main_dict_infourl)
								+ space
								+ currentDictionary.originURL
								+ crlf
								+ res.getString(R.string.main_dict_infolic)
								+ space
								+ currentDictionary.license
								+ crlf
								+ res.getString(R.string.main_dict_infodwl)
								+ space
								+ DOWNLOAD_HOST // TODO to be changed with the
												// download link
								+ crlf
								+ crlf
								+ res.getString(R.string.main_dict_infoword)
								+ space
								+ currentDictionary.wordCount
								+ crlf
								+ res.getString(R.string.main_dict_infosize)
								+ space
								+ Utils.getFormatedSize((null != df ? df.size
										: -1)))
				.setTitle(res.getString(R.string.main_dict_infotitle)).show();

		TextView textView = (TextView) dialog
				.findViewById(android.R.id.message);
		Linkify.addLinks(textView, Linkify.WEB_URLS);
		textView.setTextSize(12);

		dialog.show();
	}

	/**
	 * Force the display of the software keyboard
	 */
	private void showSoftKeyboard() {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	/**
	 * This will start the device camera which helps you to take a photo into
	 * the specified file
	 * 
	 * The camera activity returns into the {@link #onActivityResult} method
	 */
	private void startCameraActivity() {
		boolean ocrOk = isTesseractInstalled(currentDictionary.source_language);
		if (!ocrOk)
			installOCRLibDialog(currentDictionary.source_language);

		if (ocrOk) {
			// installTrainedData(currentDictionary.source_language);

			Intent intent = new Intent(
					android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

			mImageCaptureUri = Uri
					.fromFile(Utils.getJPEGFile(this, "/ocr.jpg"));
			intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

			startActivityForResult(intent, 0);
		}
	}

	/**
	 * A generic test case that can be used to check if all the words in the
	 * current dictionary can be found and displayed in the detail page
	 */
	private void test() {
		if (currentDb != null && currentDb.isOpen()) {

			final Cursor c1 = currentDb.rawQuery(
					"select word_id,value from word;", null);

			c1.moveToFirst();
			try {
				while (!c1.isAfterLast()) {
					// Log.println(
					// Log.DEBUG,
					// "test",
					// String.format("word_id=%d (%s)", c1.getInt(0),
					// c1.getString(1)));

					edtSearch.setText(c1.getString(1));
					final Intent detail = new Intent(Main.this, Detail.class);
					showDialog(LOADING_DIALOG);

					Word word = getWord(c1.getInt(0),
							appPref.detailPrintOption, null);

					detail.putExtra("se.lexicon.android.start_time", 0);
					detail.putExtra("se.lexicon.android.detail", word);
					detail.putExtra("title", getString(R.string.app_title)
							+ "-"
							+ getString(R.string.menu_license).toLowerCase());
					startActivityForResult(detail, word.word_id);
					finishActivity(word.word_id);

					try {
						// give the CPU cores time to swap with each other, they
						// will thank you!
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					c1.moveToNext();
				}
			} finally {
				c1.close();
			}
		}
	}

	/**
	 * Generic procedure that launch a specific test case
	 * 
	 * @param no
	 *            the internal number of the test case
	 */
	private void testCase(int no) {
		if (!_ENABLE_TEST_CASE)
			return;

		test();
	}

	/**
	 * Traverse the xmlFile and return a list of dictionary files
	 * 
	 * @param xmlFile
	 *            the file to be parsed
	 * @return a List of {@code DictionaryFile} as defined in the xmlFile
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private List<DictionaryFile> traverse(File xmlFile)
			throws ParserConfigurationException, SAXException, IOException {
		DictionarySaxParser handler = null;
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		handler = new DictionarySaxParser();
		parser.parse(xmlFile, handler);

		return handler.dictionaries;
	}

	/**
	 * Remove a dictionary database from the sdcard and from application
	 * preferences
	 * 
	 * @param dict
	 *            the dictioary to be removed
	 * @return true if dictionary removed successfully
	 */
	private boolean uninstallDictionary(final DictionaryFile dict) {
		final String dbFullPath = Utils.getDbDirectory(this);
		boolean result = false;
		if (null != dict) {
			if (dict.type != DictionaryType.MP3) {
				// make sure to close the dictionary before deleting (if already
				// opened as currentDb)
				if (null != currentDb
						&& currentDb.getPath().compareToIgnoreCase(
								dbFullPath + dict.fileName) == 0) {
					currentDb.close();
					currentDb = null;
				}

				File f = new File(dbFullPath + dict.fileName);
				if (!f.exists() || f.delete()) {
					result = appPref.delDictionary(dict.fileName);
				}
			} else {
				Resources res = getResources();
				final ProgressDialog progDialog = new ProgressDialog(this);
				progDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				progDialog.setTitle(R.string.main_dwl_wait);
				progDialog.setMessage(res
						.getString(R.string.progress_delete_files));
				progDialog.setCancelable(false);
				progDialog.show();
				new Thread() {
					public void run() {
						try {
							Utils.rmFile(new File(dbFullPath
									+ Utils.MP3_LOCATION),
									new IProgressUpdater() {

										@Override
										public void setStatus(String status) {
										}

										@Override
										public void setProgress(int position,
												String details, long start) {
											progDialog.setProgress(position);
										}

										@Override
										public void setMaxPosition(int maxpos) {
											progDialog.setMax(maxpos);

										}
									});
							appPref.delDictionary(dict.fileName);
						} catch (IOException e) {
						}
						progDialog.dismiss();
					}
				}.start();
				result = true;
			}
		}
		return result;
	}

	/**
	 * Provides a dialog which suggests the available dictionaries to be
	 * uninstalled. If anything is chosen then they will be uninstalled.
	 */
	private void uninstallDictionaryDialog(DialogInterface dialog) {
		final ArrayList<String> items = new ArrayList<String>();
		final Resources res = getResources();
		final List<DictionaryFile> instDictionary = getInstalledDictionaries();

		if (null != dialog)
			dialog.dismiss();

		for (int j = 0; j < instDictionary.size(); j++)
			items.add(instDictionary.get(j).name);

		AlertDialog alert = null;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(res.getString(R.string.main_dict_installed));
		builder.setCancelable(true);
		final boolean[] checked = new boolean[items.size()];
		builder.setMultiChoiceItems(
				items.toArray(items.toArray(new CharSequence[items.size()])),
				checked, new OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						checked[which] = isChecked;
					}
				});
		builder.setPositiveButton(res.getString(R.string.btn_continue),
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (null != checked)
							for (int i = checked.length - 1; i >= 0; i--)
								if (checked[i])
									if (uninstallDictionary(instDictionary
											.get(i))) {
										appPref.setDictionaryIndex(appPref
												.getDictIndex());
										initApp();
									}
						dialog.dismiss();
					}
				});

		alert = builder.create();
		alert.show();
	}

	/**
	 * Download and install last version of apk
	 */
	private void updateApp() {
		try {
			new LiveUpdate(Main.this, 0, DOWNLOAD_ROOT + "/lexicon.apk").run();
		} catch (LiveUpdException e) {
			Utils.optionNotAvailable(Main.this);
		} catch (IOException e) {
			LogError(e.getMessage());
		}
	}

	/**
	 * Download and update the installed dictinaries
	 * 
	 * @param dict
	 *            the dictionary to be updated
	 * @return true if updated successfully
	 */
	private boolean updateDictionary(DictionaryFile dict) {
		boolean result = true;
		try {
			if (dict.type == DictionaryType.WORD)
				result = result
						&& installDictionaryFromWeb(dict,
								Utils.getDbDirectory(this));
			else if (dict.type == DictionaryType.MP3)
				result = result
						&& installDictionaryFromWeb(dict,
								Utils.getDbDirectory(this) + Utils.MP3_LOCATION);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Update the word ussage statistic in the database
	 * 
	 * @param word_id
	 *            the Id corresponding to the word whose statistic ussage will
	 *            be incremented
	 */
	private void updateStatistics(Integer word_id) {
		if (!(null != currentDb && currentDb.isOpen()))
			return;
		currentDb
				.execSQL("insert into word_statistics select "
						+ String.valueOf(word_id)
						+ ",0 where not exists(select * from word_statistics where word_id="
						+ String.valueOf(word_id) + ")");
		currentDb
				.execSQL("update word_statistics set stat_count=stat_count+1 where word_id="
						+ String.valueOf(word_id));

	}
}