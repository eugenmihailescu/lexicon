package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		License.java $
 * 
 * @id:	License.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.util.Linkify;
import android.text.util.Linkify.TransformFilter;
import android.widget.TextView;

public class License extends Activity {

	private void handleBitbucketLinks(TextView txtView) {
		// TODO make the pattern to not match other stuff in the html text
		final String bitbucketUrl = "https://bitbucket.org/eugenmihailescu/lexicon/changeset/";
		TransformFilter revisionTransformer = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {

				return bitbucketUrl + match.group().substring(3);
			}
		};

		Linkify.addLinks(txtView, Pattern.compile("\\d.\\d-rc?-\\d-\\w{1,8}"),
				null, null, revisionTransformer);
	}

	private void handleDictionaryLinks(TextView txtView) {
		final String[] urls = {
				"http://folkets-lexikon.csc.kth.se/folkets/om.html",
				"http://creativecommons.org/licenses/by-sa/2.5",
				"http://lexin2.nada.kth.se/lexin", "http://www.sprakradet.se",
				"http://www.kth.se" };
		final String[] patterns = {
				"Folkets lexikon",
				"Distributed Creative Commons Attribution-Share Alike 2.5 Generic license",
				"Lexin website", "Språkrådet", "Kungliga Tekniska högskolan" };
		TransformFilter revisionTransformer = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {
				String tUrl = "";
				for (int i = 0; i < patterns.length; i++)
					if (match.group().compareTo(patterns[i]) == 0) {
						tUrl = urls[i];
						break;
					}
				return tUrl;
			}
		};

		String pattern = "";
		for (String s : patterns)
			pattern += (pattern.length() > 0 ? "|" : "") + s;

		Linkify.addLinks(txtView, Pattern.compile(pattern), null, null,
				revisionTransformer);
	}

	private void handleEmailLinks(TextView txtView) {
		// TODO make the pattern to not match other stuff in the html text
		final String emailUrl = "mailto:%s";
		TransformFilter revisionTransformer = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {

				return String.format(emailUrl, match.group());
			}
		};

		Linkify.addLinks(
				txtView,
				Pattern.compile("\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}\\b"),
				null, null, revisionTransformer);
	}

	private void handleTessLinks(TextView txtView) {
		final String[] urls = {
				"http://www.apache.org/licenses/LICENSE-2.0.html",
				"https://github.com/rmtheis/tess-two",
				"http://code.google.com/p/tesseract-ocr",
				"http://www.leptonica.org", "http://libjpeg.sourceforge.net" };
		final String[] patterns = { "Apache License, Version 2.0", "tess-two",
				"Tesseract 3.0.1", "Leptonica 1.68", "LibJPEG 6b" };
		TransformFilter revisionTransformer = new TransformFilter() {

			@Override
			public String transformUrl(Matcher match, String url) {
				String tUrl = "";
				for (int i = 0; i < patterns.length; i++)
					if (match.group().compareTo(patterns[i]) == 0) {
						tUrl = urls[i];
						break;
					}
				return tUrl;
			}
		};

		String pattern = "";
		for (String s : patterns)
			pattern += (pattern.length() > 0 ? "|" : "") + s;

		Linkify.addLinks(txtView, Pattern.compile(pattern), null, null,
				revisionTransformer);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.license);
		setTitle(getIntent().getExtras().getString("title"));

		/**
		 * Revision information is automatically updated in the constant strings
		 * bellow on git checkout or when Ant build.xml is run
		 */
		String gitRevision = String.valueOf("1.1-rc-1-ga23412c");
		String gitChanged = String.valueOf("@short_date:	2012-05-20 $")
				.substring(13);
		gitChanged = gitChanged.substring(0, gitChanged.length() - 2);

		TextView txtVersion = (TextView) findViewById(R.id.txtVersion);
		TextView txtlicenseInfoMiddle = (TextView) findViewById(R.id.licenseInfoMiddle);

		String gitAuthor = String.valueOf(
				"@author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $")
				.substring(10);
		gitAuthor = gitAuthor.substring(0, gitAuthor.indexOf('<') - 1);

		String gitEmail = String.valueOf(
				"@email:		eugenmihailescux@gmail.com $").substring(9);
		gitEmail = gitEmail.substring(0, gitEmail.length() - 2);

		Resources res = getResources();
		String fmt = res.getString(R.string.licenseInfoTop);

		txtVersion.setText(String.format(fmt, gitRevision, gitChanged,
				gitEmail, gitAuthor));

		handleBitbucketLinks(txtVersion);
		handleEmailLinks(txtVersion);
		handleTessLinks(txtlicenseInfoMiddle);
		handleDictionaryLinks(txtlicenseInfoMiddle);
	}
}
