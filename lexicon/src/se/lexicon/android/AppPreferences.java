package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		AppPreferences.java $
 * 
 * @id:	AppPreferences.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Class used to save/load the user preferences
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class AppPreferences {
	private Context context;

	public Boolean listSortedAscendent = true;

	public Set<PrintOptions> detailPrintOption;
	public int searchTopResult = 100;
	private int dictIndex = -1;

	public Boolean searchInPlace = true;
	public Boolean searchDeep = false;
	public Boolean showTranslation = true;
	public Boolean clearByShaking = false;
	public int dwlBufferSize = 16 * 1024;

	public List<DictionaryFile> items;
	private List<String> names;
	public boolean createStatistics;

	AppPreferences(Context context) {
		this.context = context;
		items = new ArrayList<DictionaryFile>();
		names = new ArrayList<String>();
		refresh();
	}

	/**
	 * Add to the application shared preferences Map a new dictionary setting
	 * with the specified characteristics. If they already exists(?!) them
	 * values will be updated.
	 * 
	 * @param dictName
	 *            the friendly name of the dictionary
	 * @param dictFile
	 *            the file name (including extension, if there exists) of the
	 *            dictionary as it exists on the web available for download and
	 *            as it would be saved locally on the sdcard
	 * @param dictParts
	 *            Number of parts that the dictionary is splitted and available
	 *            for download. If the dictionary is large enough it could be
	 *            splitted in many parts using the Linux (or equivalent) command
	 *            split.
	 * 
	 *            Ex: split -b 1MB my_dictionary my_dictFile_
	 * 
	 *            The split command will add a prefix like
	 *            aa,ab,ac,....za,zb,...zz to each splitted part.
	 * @param dictUrl
	 *            the full URL path there the dictFile could be downloaded
	 * @param type
	 *            the type of the dictionary to be added
	 * @param archive
	 *            true if the specified dictionary is zipped
	 * @param dictSize
	 *            the number of bytes of the dictionary (if known)
	 * @param dictItemCount
	 *            the number of items (such as words/files) that are logically
	 *            included in the dictionary
	 * 
	 */
	public void addDictionary(String dictName, String dictFile, int dictParts,
			String dictUrl, DictionaryType type, boolean archive, int dictSize,
			int dictItemCount) {

		// if it already exists then delete the existent version
		delDictionary(dictFile);

		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();

		int lastDictIndex = 0;
		while (pref.contains(String.format("dict%d.file", lastDictIndex)))
			lastDictIndex++;

		editor.putString(String.format("dict%d.name", lastDictIndex), dictName);
		editor.putString(String.format("dict%d.file", lastDictIndex), dictFile);
		editor.putInt(String.format("dict%d.parts", lastDictIndex), dictParts);
		editor.putString(String.format("dict%d.url", lastDictIndex), dictUrl);
		editor.putInt(String.format("dict%d.size", lastDictIndex), dictSize);
		editor.putInt(String.format("dict%d.itemCount", lastDictIndex),
				dictItemCount);
		editor.putBoolean(String.format("dict%d.archive", lastDictIndex),
				archive);
		editor.putString(String.format("dict%d.type", lastDictIndex),
				type.name());
		if (editor.commit()) {
			items.add(new DictionaryFile(lastDictIndex, dictName, dictFile,
					dictParts, dictUrl, type, archive, dictSize, dictItemCount));
			names.add(dictName);
		}
	}

	/**
	 * Delete from the preferences (and also from the internally maintained
	 * items/file names collections) the name/file/parts/url settings that
	 * correspond to the specified dictFile file name
	 * 
	 * @param dictFile
	 *            the dictionary file name to be removed
	 * @return true if found and removed from application preferences
	 */
	public boolean delDictionary(String dictFile) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);

		String key = getPreferenceKeyByValue(dictFile);
		if (null != key) {
			int index = getPreferenceDictIdByKey(key);
			if (index > -1) {
				SharedPreferences.Editor editor = pref.edit();
				editor.remove(String.format("dict%d.name", index));
				editor.remove(String.format("dict%d.file", index));
				editor.remove(String.format("dict%d.parts", index));
				editor.remove(String.format("dict%d.url", index));
				editor.remove(String.format("dict%d.size", index));
				editor.remove(String.format("dict%d.itemCount", index));
				editor.remove(String.format("dict%d.archive", index));
				editor.remove(String.format("dict%d.type", index));
				editor.commit();

				remvoveItemByFileName(dictFile);

				if (dictIndex >= items.size() && items.size() > 0)
					setDictionaryIndex(0);
				else if (items.size() < 1 && dictIndex > -1)
					setDictionaryIndex(-1);

				return true;
			}
		}
		return false;
	}

	/**
	 * Return the index of the current dictionary (it is supposed that the
	 * application's user is just working with this dictionary)
	 * 
	 * @return the id of the dictionary that is currently used or 0 if there are
	 *         installed dictionaries but it seems that the user have not choose
	 *         a particular one; so the first (index) in the list is returned;
	 */
	public int getDictIndex() {
		// if -1 but there are plenty of dictionaries
		if (items.size() > 0 && dictIndex < 0)
			return 0; // use the first one by default
		else if (dictIndex >= items.size()) // if is beyond the installed
											// dictionary limits
			if (items.size() > 0) // do we have at least one dictionary?
				return 0; // return the first one by default
			else
				return -1; // no dictionary so -1 will be the logical value
		else
			return dictIndex; // whatever...
	}

	public CharSequence[] getDictionaryNames(DictionaryType type) {
		Vector<CharSequence> v = new Vector<CharSequence>();
		for (int i = 0; i < items.size(); i++)
			if (items.get(i).type == type)
				v.add(names.get(i));
		CharSequence[] result = new CharSequence[v.size()];
		v.toArray(result);
		return result;
	}

	/**
	 * A dictionary key name has the following pattern: dict<index>.<field>
	 * where <index> is the unique index of the dictionary that was saved and
	 * the <field> is one of the following: file, name, parts, url
	 * 
	 * @param key
	 * @return a zero based index if it can decipher the <index> part from the
	 *         specified string key or -1 in case it can not
	 */
	private int getPreferenceDictIdByKey(String key) {
		int pos = key.indexOf(".");
		int index = -1;
		if (pos > -1)
			index = Integer.parseInt(key.substring(4, pos));
		return index;
	}

	/**
	 * Search on the preference's Map for the first appearance of the specified
	 * value and then returns the key that correspond to that value. It is used
	 * internally whenever there is a need to update/remove a key in the
	 * preference Map. To be used only when you're sure that the value is uniq
	 * within the preference's values
	 * 
	 * @param fileName
	 * @return the first key name that corresponds to that value
	 */
	private String getPreferenceKeyByValue(String fileName) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);
		String result = null;
		int i = 0;
		boolean found = false;
		while (!found && i < items.size()) {
			if (items.get(i).fileName.compareToIgnoreCase(fileName) == 0) {
				for (Entry<String, ?> entry : pref.getAll().entrySet()) {
					if (entry.getValue().toString()
							.compareToIgnoreCase(items.get(i).fileName) == 0) {
						result = entry.getKey();
						break;
					}
				}
			}
			if (found)
				break;
			else
				i++;
		}
		return result;
	}

	/**
	 * Read and updates class properties base on the application saved
	 * preferences
	 */
	public void refresh() {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);

		searchTopResult = pref.getInt("searchTopResult", 100);
		searchInPlace = pref.getBoolean("search_inplace", true);
		searchDeep = pref.getBoolean("deep_search", false);
		detailPrintOption = Utils.getPrintOptions(context);
		showTranslation = pref.getBoolean("show_translation", true);
		dictIndex = pref.getInt("dictIndex", -1);
		clearByShaking = pref.getBoolean("clear_by_shaking", true);
		createStatistics = pref.getBoolean("create_statistics", false);
		dwlBufferSize = pref.getInt("download_buffer", 16 * 1024);

		items.clear();
		names.clear();

		// we will scan for dictionaries from index 0 -> 100
		// we admit that 100 is a huge number for installed dictionaries
		int scanLimit = 100;
		int index = 0;
		DictionaryType dictType;
		int dictParts, dictSize, dictItemCount;
		String dictName, dictFile, dictUrl, p;
		boolean dictArch;
		while (scanLimit-- > 0) {
			if (pref.contains(String.format("dict%d.file", index))) {
				p = String.format("dict%d.", index);
				dictName = pref.getString(p + "name", "");
				dictFile = pref.getString(p + "file", "");
				dictParts = pref.getInt(p + "parts", 1);
				dictUrl = pref.getString(p + "url", "");
				dictType = DictionaryType.valueOf(pref.getString(p + "type",
						DictionaryType.WORD.name()));
				dictArch = pref.getBoolean(p + "archive", false);
				dictSize = pref.getInt(p + "size", -1);
				dictItemCount = pref.getInt(p + "itemCount", -1);
				items.add(new DictionaryFile(index, dictName, dictFile,
						dictParts, dictUrl, dictType, dictArch, dictSize,
						dictItemCount));
				names.add(dictName);
			}
			index++;
		}
		if (dictIndex < 0 && items.size() > 0)
			setDictionaryIndex(0);
		else if (items.size() < 1)
			setDictionaryIndex(-1);
	}

	/**
	 * Items and names are two collections of strings used internally to provide
	 * to the other classes a change to work easily with the dictionary's
	 * names/file names
	 * 
	 * @param dictFile
	 */
	private void remvoveItemByFileName(String dictFile) {
		for (int i = 0; i < items.size(); i++)
			if (items.get(i).fileName.compareToIgnoreCase(dictFile) == 0) {
				items.remove(i);
				names.remove(i);
				break;
			}
	}

	/**
	 * Set and save to preference file the specified dictionary index
	 * 
	 * @param index
	 */
	public void setDictionaryIndex(int index) {
		dictIndex = index;
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		editor.putInt("dictIndex", dictIndex);
		editor.commit();
	}
}