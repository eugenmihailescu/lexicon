package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		Utils.java $
 * 
 * @id:	Utils.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Class which offers few usefull tools like playing a media file, downloading a
 * file, checking the net state, extended version of Toast, etc...
 * 
 * @author "Eugen Mihailescu"
 * 
 */
public class Utils {

	final static String ARCHIVE_TYPE = "zip";
	final static int HTTP_TIMEOUT = 30000; // 1 min
	public final static String MP3_LOCATION = "mp3/";

	private static final int KB = 1024;
	private static final int MB = KB * KB;
	private static final int GB = KB * MB;
	// every 100 reads (the download spead decress serious as this number
	// lowers because the CPU is busy with painting the screen instead of brute
	// work; i.e: @upd_freq=1000 is 4x times faster than @upd_freq=1)
	final static int UPDATE_PROGRESS_FREQ = 100;// good balance CPU/quality

	/**
	 * Copy a file from source to destionation
	 * 
	 * @param src
	 *            the source File to be copied
	 * @param dst
	 *            the destination File where to put the copied File
	 * @throws IOException
	 */
	private static void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

	/**
	 * Download one/many files from the urlRoot and save them locally (on
	 * sdcard) as outputFile. The file(s) that will be downloaded are those
	 * whose file name are specified in parts parameter.
	 * 
	 * @param urlRoot
	 *            the url where the parts file should be found
	 * @param parts
	 *            one or many files that will be downloaded and concatenated as
	 *            outputFile
	 * @param outputFile
	 *            the file name where all the parts will be concatenated and
	 *            stored locally. This file will be in fact a sqlite database
	 *            where a dictionary will be stored.
	 * @param onProgress
	 *            an event which could be used to notify the download progress
	 * @throws IOException
	 */
	public static boolean DownloadOnSDcard(Context context,
			int DOWNLOAD_BUFFER_SIZE, String urlRoot, ArrayList<String> parts,
			String outputFile, boolean archive, IProgressUpdater onProgress,
			IOnDownloadCanceled onCanceled) throws IOException {

		boolean result = true;

		String tempDownloadPath = Environment.getExternalStorageDirectory()
				+ "/download/";
		String tempDownloadFile = (new File(outputFile).getName());
		File fileTmp = null;
		byte[] buffer = new byte[DOWNLOAD_BUFFER_SIZE];

		for (int i = 0; i < parts.size(); i++) {

			// done is used by the next thread, to add its count to the one
			// already done by the previous concurent threads
			int done = 0;

			URL url = new URL(urlRoot + "/" + parts.get(i)
					+ (archive ? "." + ARCHIVE_TYPE : ""));

			HttpURLConnection httpConn = (HttpURLConnection) url
					.openConnection();
			httpConn.setRequestMethod("GET");
			httpConn.setConnectTimeout(HTTP_TIMEOUT);
			httpConn.setReadTimeout(HTTP_TIMEOUT);

			httpConn.setDoOutput(true);
			httpConn.connect();

			if (null != onProgress) {
				onProgress.setProgress(done, null, 0);
				onProgress.setMaxPosition(httpConn.getContentLength());
			}

			File tempDir = new File(tempDownloadPath);
			if (!tempDir.exists()) {
				tempDir.mkdirs();
			}

			// make sure that temp file and dest file are not the same
			String tmpName = tempDir + "/" + tempDownloadFile
					+ (archive ? "." + ARCHIVE_TYPE : "");
			if (tmpName.compareToIgnoreCase(outputFile) == 0)
				tmpName += "_";

			// first we download to a temporarly location
			fileTmp = new File(tmpName);

			// append each part to the same destination file
			FileOutputStream fos = new FileOutputStream(fileTmp, i > 0
					&& fileTmp.exists() && !archive);

			InputStream is = httpConn.getInputStream();
			// TODO treat exception on stream read error(e.g: when net fail)
			int bRead = 0;
			byte progUpdCounter = 0;
			int bCumulatedRead = 0;
			IOException err = null;
			try {
				while ((bRead = is.read(buffer)) != -1) {
					if (null != onCanceled && onCanceled.isCanceled()) {
						result = false;
						break;
					}
					fos.write(buffer, 0, bRead);

					// the code bellow is about the progress notification
					if (null != onProgress) {
						bCumulatedRead += bRead;
						if (progUpdCounter++ >= UPDATE_PROGRESS_FREQ) {
							done += bCumulatedRead;
							onProgress.setProgress(bCumulatedRead, null, 0);
							bCumulatedRead = 0;
							progUpdCounter = 0;
						}
					}
				}
			} catch (IOException e) {
				err = e;
				result = false;
			}

			if (null != onProgress && bCumulatedRead > 0)
				onProgress.setProgress(bCumulatedRead, null, 0);

			fos.close();
			is.close();

			if (null != err) {
				fileTmp.delete();
				throw err;
			} else if (archive)
				installFile(context, fileTmp, outputFile, archive, onProgress);

		}

		if (result && !archive)
			installFile(context, fileTmp, outputFile, archive, null);

		return result;
	}

	/**
	 * Returns the path of the folder where sqlite application database are
	 * stored
	 * 
	 * @return a string representing the database folder name
	 */
	static String getDbDirectory(Context context) {
		return Utils.getSdCardPath() + "/Android/data/"
				+ context.getPackageName() + "/files/";
	}

	/**
	 * Try to get from the download server the file size of each parts file that
	 * will be downloaded
	 * 
	 * @param urlRoot
	 *            the root folder where the file mentioned into parts is located
	 * @param parts
	 *            the array of files that have to be downloaded from urlRoot
	 * @param archive
	 *            if the files mentioned into parts is .zip archive
	 * @return the total size of the files to be downloaded
	 * @throws IOException
	 */
	public static int getDownloadSize(String urlRoot, ArrayList<String> parts,
			boolean archive) throws IOException {

		int result = 0;

		for (int i = 0; i < parts.size(); i++) {

			URL url = new URL(urlRoot + "/" + parts.get(i)
					+ (archive ? "." + ARCHIVE_TYPE : "")); // Your given URL.

			HttpURLConnection httpConn = (HttpURLConnection) url
					.openConnection();
			httpConn.setRequestMethod("GET");
			httpConn.setConnectTimeout(HTTP_TIMEOUT);
			httpConn.setReadTimeout(HTTP_TIMEOUT);
			httpConn.setDoOutput(true);
			httpConn.connect(); // Connection Complete here.!

			result += httpConn.getContentLength();
		}

		return result;
	}

	/**
	 * Format a file size specified in bytes and return the corresponding value
	 * in KB/MB/GB/TB
	 * 
	 * @param n
	 *            the size of bytes to be formated
	 * @return a string containing the formated size
	 */
	public static String getFormatedSize(float n) {
		n = n < 0 ? 0 : n;
		if ((n / KB) < KB)
			return String.format("%.2f KB", n / KB);
		else if ((n / MB) < MB)
			return String.format("%.2f MB", n / MB);
		else if ((n / GB) < GB)
			return String.format("%.2f GB", n / GB);
		else
			return String.format("%.2f TB", n / KB * GB);
	}

	/**
	 * Returns the path of the folder where temporary files are stored
	 * 
	 * @return a string representing the folder name
	 */
	static File getJPEGFile(Context context, String fileName) {
		String x = Utils.getSdCardPath() + "/Android/data/"
				+ context.getPackageName() + "/files/temp/";
		File f = new File(x);
		f.mkdirs();
		return new File(f.getAbsoluteFile() + fileName);
	}

	/**
	 * Read the application SharedPreferences for PrintOption user preferences
	 * 
	 * @param context
	 * @return a Set of PrintOptions as they were choosen by the user
	 */
	static Set<PrintOptions> getPrintOptions(Context context) {
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);

		Set<PrintOptions> result = new LinkedHashSet<PrintOptions>();

		if (pref.getBoolean("detail_level_fulloption", true))
			for (PrintOptions opt : PrintOptions.values())
				result.add(opt);
		else {
			if (pref.getBoolean("detail_level_basic", true))
				result.add(PrintOptions.BASIC);

			if (pref.getBoolean("detail_level_phonetic", true))
				result.add(PrintOptions.PHONETIC);

			if (pref.getBoolean("detail_level_grammar", true))
				result.add(PrintOptions.GRAMMAR);

			if (pref.getBoolean("detail_level_definition", true))
				result.add(PrintOptions.DEFINITION);

			if (pref.getBoolean("detail_level_compound", true))
				result.add(PrintOptions.COMPOUND);

			if (pref.getBoolean("detail_level_use", true))
				result.add(PrintOptions.USE);

			if (pref.getBoolean("detail_level_derivation", true))
				result.add(PrintOptions.DERIVATION);

			if (pref.getBoolean("detail_level_variant", true))
				result.add(PrintOptions.VARIANT);

			if (pref.getBoolean("detail_level_idiom", true))
				result.add(PrintOptions.IDIOM);

			if (pref.getBoolean("detail_level_inflection", true))
				result.add(PrintOptions.INFLECTION);

			if (pref.getBoolean("detail_level_synonym", true))
				result.add(PrintOptions.SYNONYM);

			if (pref.getBoolean("detail_level_related", true))
				result.add(PrintOptions.RELATED);

			if (pref.getBoolean("detail_level_example", true))
				result.add(PrintOptions.EXAMPLE);

			if (pref.getBoolean("detail_level_explanation", true))
				result.add(PrintOptions.EXPLANATION);

			if (pref.getBoolean("detail_level_picture", true))
				result.add(PrintOptions.PICTURE);

			if (pref.getBoolean("detail_level_see", true))
				result.add(PrintOptions.SEE);
		}
		return result;
	}

	/**
	 * Calculate and return the available space on SDCard
	 * 
	 * @return number of free bytes available on SDCard
	 */
	public static long getSDCardFreeSpace() {
		StatFs fs = new StatFs(getSdCardPath());
		return (long) fs.getAvailableBlocks() * (long) fs.getBlockSize();
	}

	/**
	 * @return a string to the full path (eg: /sdcard) of phone sdcard
	 */
	static String getSdCardPath() {
		String sdcard_state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(sdcard_state)
				|| Environment.MEDIA_MOUNTED_READ_ONLY.equals(sdcard_state))
			return Environment.getExternalStorageDirectory().getPath();
		else
			return null;
	}

	static String getTessdataPath(Context context) {
		return Utils.getSdCardPath() + "/Android/data/"
				+ context.getPackageName() + "/files/tessdata/";
	}

	/**
	 * Move/unzip the downloaded file to its final location
	 * 
	 * @param fileTmp
	 *            the temporary file downloaded
	 * @param outputFile
	 *            the output file/path where to move/unzip the file downloaded
	 * @param archive
	 *            true if the temporary file is archive
	 * @param onProgress
	 *            event to call during the install
	 * @throws IOException
	 */
	private static void installFile(Context context, File fileTmp,
			String outputFile, boolean archive, IProgressUpdater onProgress)
			throws IOException {
		if (null != fileTmp) {
			// create the parent directory, otherwise we'll have problems...
			File file = new File(new File(outputFile).getParent());
			if (!file.exists())
				file.mkdirs();

			// last but not least, move the temp to the its final location
			if (!archive)
				copyFile(fileTmp, new File(outputFile));
			else
				unzip(context, fileTmp, file, onProgress);

			fileTmp.delete();
		}
	}

	/**
	 * @param activity
	 * @return true if connected to network (not especially Internet)
	 */
	static boolean isOnline(Activity activity) {
		ConnectivityManager cm = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	/**
	 * Show a Toast dialog with a custom border layout with main app icon,
	 * displayed for a LENGTH_LONG period
	 * 
	 * @param activity
	 * @param msg
	 */
	static void lexToast(Activity activity, String msg) {
		lexToast(activity, msg, R.drawable.ic_launcher);
	}

	/**
	 * Show a Toast dialog with a custom border layout, using the main app icon
	 * and displayed for a LENGTH_LONG period
	 * 
	 * @param msg
	 * @param rIcon
	 */
	static void lexToast(Activity activity, String msg, int rIcon) {
		LayoutInflater inflater = activity.getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast,
				(ViewGroup) activity.findViewById(R.id.toast_layout_root));

		ImageView image = (ImageView) layout.findViewById(R.id.image);
		image.setImageResource(rIcon);
		TextView text = (TextView) layout.findViewById(R.id.text);
		text.setText(msg);

		Toast toast = new Toast(activity);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(layout);
		toast.show();
	}

	/**
	 * Show a Toast dialog with a custom border layout, using specified icon
	 * resource id
	 * 
	 * @param activity
	 * @param msg
	 * @param rIcon
	 */
	static void lexToast1(Activity activity, String msg, int rIcon) {
		lexToast(activity, msg, rIcon);
	}

	/**
	 * Show a Toast dialog with a custom border layout with a standard message
	 * (Option not available) using a error icon
	 * 
	 * @param activity
	 */
	static void optionNotAvailable(Activity activity) {

		lexToast(
				activity,
				activity.getResources().getString(
						R.string.class_utils_optionnotavail),
				android.R.drawable.ic_menu_report_image);
	}

	/**
	 * Play the media file specified, in case of error display a Toast error
	 * message
	 * 
	 * @param activity
	 * @param mediaFile
	 */
	static void playMedia(final Activity activity, String mediaFile) {
		File file = new File(mediaFile);
		if (!file.exists()) {
			lexToast1(activity,
					String.format(
							activity.getResources().getString(
									R.string.class_utils_medianotexists),
							mediaFile), android.R.drawable.ic_lock_silent_mode);
			return;
		}
		try {
			MediaPlayer mp = new MediaPlayer();
			mp.setOnErrorListener(new OnErrorListener() {

				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
					lexToast1(
							activity,
							String.format(
									activity.getResources().getString(
											R.string.class_utils_mediaerror),
									what == MediaPlayer.MEDIA_ERROR_UNKNOWN ? "MEDIA_ERROR_UNKNOWN"
											: "MEDIA_ERROR_SERVER_DIED", extra),
							R.drawable.ic_volume_off_small);
					return false;
				}
			});
			mp.reset();
			mp.setDataSource(mediaFile);
			mp.prepare();

			mp.start();

		} catch (Exception e) {
			lexToast1(
					activity,
					String.format(
							activity.getResources().getString(
									R.string.class_utils_mediaerror1),
							e.getMessage()), R.drawable.ic_volume_off_small);
		}
	}

	/**
	 * Extract files included into zipFile at the specified outputPath
	 * 
	 * @param zipFile
	 *            the archive beeing unzipped
	 * @param outputPath
	 *            the path where files are written
	 * @param onProgress
	 *            the event to call for updating the status
	 * @throws IOException
	 */
	private static void unzip(Context context, File zipFile, File outputPath,
			IProgressUpdater onProgress) throws IOException {

		byte[] buffer = new byte[4096]; // we suppose that the mp3 files
										// contains ~4-5K of data; if it's true
										// then 4k buffer works faster!
		int bRead = 0;
		FileInputStream fin = new FileInputStream(zipFile);
		ZipInputStream zin = new ZipInputStream(fin);
		ZipEntry ze = null;
		while ((ze = zin.getNextEntry()) != null) {
			if (null != onProgress)
				onProgress.setStatus(context.getResources().getString(
						R.string.class_utils_extracting)
						+ " " + ze.getName());
			FileOutputStream fout = new FileOutputStream(outputPath.getPath()
					+ "/" + ze.getName());
			while ((bRead = zin.read(buffer)) != -1)
				fout.write(buffer, 0, bRead);
			zin.closeEntry();
			fout.close();
		}
		zin.close();

	}

	/**
	 * Remove a file or recursively a directory and its content
	 * 
	 * @param the
	 *            File/Directory that should be removed
	 * @throws IOException
	 */
	static void rmFile(File f, IProgressUpdater p) throws IOException {
		if (f.isDirectory()) {
			File[] files = f.listFiles();
			if (null != p)
				p.setMaxPosition(files.length);
			for (int i = 0; i < files.length; i++) {
				if (null != p)
					p.setProgress(i, null, 0);

				rmFile(files[i], p);
			}
		}
		if (f.exists() && !f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}
}
