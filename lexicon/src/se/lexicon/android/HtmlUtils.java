package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		HtmlUtils.java $
 * 
 * @id:	HtmlUtils.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

enum FontColor {
	aqua, black, blue, fuchsia, gray, green, lime, maroon, navy, olive, purple, red, silver, teal, white, yellow
}

// review other tags

/**
 * Class with specialized functions that helps you to give a plain text and get
 * a HTML-formated copy of it
 * 
 */
public class HtmlUtils {
	final private static byte tabCount = 3;
	final static String space = "&nbsp;";
	final static String br = "<br>";
	final static String divider = "<img src=\"divider_horizontal_dark\">";

	final static String tab = String.format(String.format("%%0%dd", tabCount),
			0).replace("0", space);
	final private static FontColor default_color = FontColor.black;

	final private static String tag_bold_start = "<b>";
	final private static String tag_bold_end = "</b>";
	final private static String tag_italic_start = "<i>";
	final private static String tag_italic_end = "</i>";
	final private static String tag_under_start = "<u>";
	final private static String tag_under_end = "</u>";
	final private static String tag_img_start = "<img src=\"%s\"%s>";
	final private static String tag_link_start = "<a href=\"%s\">";
	final private static String tag_link_end = "</a>";

	final private static String tag_font_start = "<font color=\"%s\"%s>";
	final private static String tag_font_end = "</font>";
	final private static String tag_bq_start = "<blockquote>";
	final private static String tag_bq_end = "</blockquote>";

	static String bold(String str) {
		return tag_bold_start + str + tag_bold_end;
	}

	static String font(String str, FontColor color) {
		return String.format(tag_font_start, getColorName(color), "") + str
				+ tag_font_end;
	}

	static String font(String str, FontColor color, int size) {
		return String.format(tag_font_start, getColorName(color), " size=\""
				+ String.valueOf(size) + "\"")
				+ str + tag_font_end;
	}

	static String font(String str, String color) {
		return String.format(tag_font_start, color, "") + str + tag_font_end;
	}

	static String font(String str, String color, int size) {
		return String.format(tag_font_start, color,
				" size=\"%" + String.valueOf(size) + "\"")
				+ str + tag_font_end;
	}

	private static String getColorName(FontColor color) {
		return color.name();
	}

	static String h(String str, FontColor color, int size) {
		return String.format("<h%d>%s</h%d>", size, font(str, color), size);
	}

	static String h(String str, String color, int size) {
		return String.format("<h%d>%s</h%d>", size, font(str, color), size);
	}

	static String image(String url) {
		return String.format(tag_img_start, url, "");
	}

	static String image(String url, String link) {
		return String.format(tag_img_start, url, (link != null
				&& link.length() > 0 ? "href=\"" + link + "\"" : ""));
	}

	static String italic(String str) {
		return tag_italic_start + str + tag_italic_end;
	};

	static String normal(String str) {
		return font(str, default_color);
	}

	static String quote(String str) {
		return tag_bq_start + str + tag_bq_end;
	}

	static String textlink(String text, String link) {
		return String.format(tag_link_start, link) + text + tag_link_end;
	}

	static String title(String str, FontColor color) {
		return br + font(bold(str + ": "), color);
	}

	static String title(String str, FontColor color, int size) {
		return br + font(bold(str + ": "), color, size);
	}

	static String title(String str, String color) {
		return br + font(bold(str + ": "), color);
	}

	static String title(String str, String color, int size) {
		return br + font(bold(str + ": "), color, size);
	}

	static String underline(String str) {
		return tag_under_start + str + tag_under_end;
	}

}
