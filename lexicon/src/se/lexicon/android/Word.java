package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		Word.java $
 * 
 * @id:	Word.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * folkets-lexicon provides the following raw values:
 * 
 * ab : adverb
 * 
 * abbrev : abbreviation
 * 
 * article : article
 * 
 * ie : infinitival marker
 * 
 * in : interjektion
 * 
 * jj : adjective
 * 
 * kn : conjunction
 * 
 * nn : noun
 * 
 * pm : proper noun
 * 
 * pn : personal and indefinite pronoun
 * 
 * pp : preposition
 * 
 * prefix : prefix in compound
 * 
 * rg : cardinal number
 * 
 * suffix : suffix
 * 
 * vb : verb
 */
enum Classes {
	UNKNOWN_CLASS, ab, abbrev, article, ie, in, jj, kn, nn, pm, pn, pp, prefix, rg, suffix, vb
}

enum Languages {
	UNKNOWN_LANG, sv, en
}

/**
 * // * NOTE : BASIC includes the following fields of word: value + comment, //
 * * wordclass, translation+comment The others represent exacty that collection
 * as the element name
 * 
 * @author "Eugen Mihailescu"
 * 
 */
enum PrintOptions {
	BASIC, COMPOUND, DEFINITION, DERIVATION, EXAMPLE, EXPLANATION, GRAMMAR, IDIOM, INFLECTION, PHONETIC, PICTURE, RELATED, SEE, SYNONYM, USE, VARIANT
}

enum RelatedTypes {
	UNKNOWN_TYPE, antonym
}

enum SeeTypes {
	UNKNOWN_TYPE, animation, compare, phonetic, saldo
}

enum URLTypes {
	UNKNOWN_TYPE, any
}

/**
 * This class provides a storage for everything that exists in the dictionary
 * about a word
 * 
 * @author "Eugen Mihailescu"
 * 
 */
public class Word implements Parcelable {

	public static String getWordClassStr(Classes w) {
		String result;
		switch (w) {
		case ab:
			result = "adverb";
			break;
		case abbrev:
			result = "abbreviation";
			break;
		case article:
			result = "article";
			break;
		case ie:
			result = "infinitival marker";
			break;
		case in:
			result = "interjektion";
			break;
		case jj:
			result = "adjective";
			break;
		case kn:
			result = "conjunction";
			break;
		case nn:
			result = "noun";
			break;
		case pm:
			result = "proper noun";
			break;
		case pn:
			result = "personal and indefinite pronoun";
			break;
		case pp:
			result = "preposition";
			break;
		case prefix:
			result = "prefix in compound";
			break;
		case rg:
			result = "cardinal number";
			break;
		case suffix:
			result = "suffix";
			break;
		case vb:
			result = "verb";
			break;
		default:
			result = w.name();
			break;
		}
		return result;
	}

	HtmlUtils html = new HtmlUtils();

	public String value;

	public String comment;

	public Classes wordClass;

	public Languages language;

	public int word_id;

	public List<WordTranslation> translation;

	public WordGrammar grammar;

	public WordPhonetic phonetic;

	public WordVariant variant;

	public List<WordSee> see;

	public List<WordDerivation> derivation;

	public List<WordURL> picture;

	public List<WordInflection> inflection;

	public List<WordSynonym> synonym;

	public WordDefinition definition;

	public WordUse use;

	public List<WordExample> example;

	public WordExplanation explanation;

	public List<WordIdiom> idiom;

	public List<WordCompound> compound;

	public List<WordRelated> related;

	public static final Parcelable.Creator<Word> CREATOR = new Parcelable.Creator<Word>() {
		@Override
		public Word createFromParcel(Parcel in) {
			return new Word(in);
		}

		@Override
		public Word[] newArray(int size) {
			return new Word[size];
		}
	};

	public Word() {
	}

	protected Word(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	private void Init() {
		translation = new ArrayList<WordTranslation>();

		see = new ArrayList<WordSee>();

		derivation = new ArrayList<WordDerivation>();

		picture = new ArrayList<WordURL>();

		inflection = new ArrayList<WordInflection>();

		synonym = new ArrayList<WordSynonym>();

		example = new ArrayList<WordExample>();

		idiom = new ArrayList<WordIdiom>();

		compound = new ArrayList<WordCompound>();

		related = new ArrayList<WordRelated>();

	}

	public void readFromParcel(Parcel in) {
		Init();

		value = in.readString();
		comment = in.readString();
		try {
			String s = in.readString();
			wordClass = Classes.valueOf(s);
		} catch (Exception e) {
			wordClass = Classes.UNKNOWN_CLASS;
		}
		try {
			String s = in.readString();
			language = Languages.valueOf(s);
		} catch (Exception e) {
			language = Languages.UNKNOWN_LANG;
		}

		word_id = in.readInt();

		in.readTypedList(translation, WordTranslation.CREATOR);

		in.readTypedList(see, WordSee.CREATOR);

		in.readTypedList(derivation, WordDerivation.CREATOR);

		in.readTypedList(picture, WordURL.CREATOR);

		in.readTypedList(inflection, WordInflection.CREATOR);

		in.readTypedList(synonym, WordSynonym.CREATOR);

		in.readTypedList(example, WordExample.CREATOR);

		in.readTypedList(idiom, WordIdiom.CREATOR);

		in.readTypedList(compound, WordCompound.CREATOR);

		in.readTypedList(related, WordRelated.CREATOR);

		grammar = in.readParcelable(WordGrammar.class.getClassLoader());

		phonetic = in.readParcelable(WordPhonetic.class.getClassLoader());

		variant = in.readParcelable(WordVariant.class.getClassLoader());

		definition = in.readParcelable(WordDefinition.class.getClassLoader());

		use = in.readParcelable(WordUse.class.getClassLoader());

		explanation = in.readParcelable(WordExplanation.class.getClassLoader());

	}

	public String toHtml(Context context, boolean isOnline) {
		return toHtml(context, isOnline, null, null, null);
	}

	/**
	 * @param isOnline
	 *            : if not then exclude those info that depends on a working
	 *            internet connection
	 * @param header
	 *            : what to output before the word html content
	 * @param footer
	 *            : what to output after the word html content
	 * @return a string that contains the HTML content for the word
	 *         full-definition
	 */
	public String toHtml(Context context, boolean isOnline, String header,
			String footer, Set<PrintOptions> options) {

		final Resources res = context.getResources();
		// word section
		final StringBuffer outBuff = new StringBuffer();

		if (header != null && header.length() > 0)
			outBuff.append(header);

		if (options == null || options != null
				&& options.contains(PrintOptions.BASIC)) {

			if (value != null && value.length() > 0)
				outBuff.append(HtmlUtils.title(value, FontColor.blue, 18));
			if (wordClass != Classes.UNKNOWN_CLASS)
				outBuff.append(HtmlUtils.font(getWordClassStr(wordClass),
						FontColor.green));
			outBuff.append(HtmlUtils.br);

			if (comment != null && comment.length() > 0)
				outBuff.append(HtmlUtils.tab + HtmlUtils.normal(comment)
						+ HtmlUtils.br);

			// word translation section
			if (translation != null & translation.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_translation),
						FontColor.red));
				final StringBuffer b = new StringBuffer();
				for (int i = 0; i < translation.size(); i++) {
					b.append(translation.get(i).value);
					if (translation.get(i).comment != null
							&& translation.get(i).comment.length() > 0)
						b.append(" (" + translation.get(i).comment + ")");
					if (i + 1 < translation.size())
						b.append(",");
				}
				outBuff.append(HtmlUtils.normal(b.toString()) + HtmlUtils.br);
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.GRAMMAR)) {
			// word grammar section
			if (grammar != null && grammar.value != null
					&& grammar.value.length() > 0)
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_grammar),
						FontColor.fuchsia)
						+ HtmlUtils.normal(grammar.value)
						+ HtmlUtils.br);
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.PHONETIC)) {
			// word phonetic section
			if (phonetic != null && phonetic.value != null
					&& phonetic.value.length() > 0)
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_pronunciation),
						"#DF3A01")
						+ HtmlUtils.normal(phonetic.value)
						+ (phonetic.soundFile != null
								&& phonetic.soundFile.length() > 0 ? HtmlUtils.space
								+ HtmlUtils.normal("mp3:play")
								+ HtmlUtils.space
								+ HtmlUtils.textlink(
										HtmlUtils.image("phonetic"),
										"mp3_play://"
												+ String.format("%d.%d.mp3",
														word_id, phonetic.id))
								: "") + HtmlUtils.br);
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.VARIANT)) {
			// word variant section
			if (variant != null && variant.value != null
					&& variant.value.length() > 0)
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_variant),
						FontColor.black)
						+ HtmlUtils.normal(variant.value
								+ (variant.alt != null
										&& variant.alt.length() > 0 ? " ("
										+ variant.alt + ")" : ""))
						+ HtmlUtils.br);
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.SEE)) {
			// word see section
			final StringBuffer seeValueBuff = new StringBuffer();
			if (see != null && see.size() > 0 && see.get(0).value.length() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_see),
						FontColor.blue));

				// see item type SALDO
				final String[] saldoVariants = { "Saldo", "association",
						"inflection" };

				final List<String> seeItems = new ArrayList<String>();

				for (int j = 0; j < see.size(); j++) {
					if (isOnline && see.get(j).type == SeeTypes.saldo) {
						final String[] seeItem = see.get(j).value
								.split("\\|\\|");
						for (int i = 0; i < seeItem.length
								&& i < saldoVariants.length; i++) {
							seeItems.add(seeItem[i]);
							seeValueBuff.append(saldoVariants[i]
									+ String.valueOf(j)
									+ (i + 1 == seeItem.length ? "" : " "));
						}
					} else if (see.get(j).type == SeeTypes.compare) {
						seeValueBuff.append(HtmlUtils.normal(see.get(j).value
								+ (j + 1 == see.size() ? "" : " ")));
					}
					if (seeValueBuff.length() > 0)
						seeValueBuff.append(HtmlUtils.br);
				}
				outBuff.append(seeValueBuff.toString());
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.DERIVATION)) {
			// word derivation section
			if (derivation != null && derivation.size() > 0) {
				final StringBuffer buff = new StringBuffer();
				buff.append(HtmlUtils.title(
						res.getString(R.string.word_section_derivation),
						FontColor.black));
				final int size = derivation.size();
				for (int i = 0; i < size; i++) {
					String d_value = derivation.get(i).value;
					if (d_value != null && d_value.length() > 0) {
						buff.append(d_value);
						if (derivation.get(i).translation != null
								&& derivation.get(i).translation.size() > 0) {
							buff.append(" (");
							final int size1 = derivation.get(i).translation
									.size();
							for (int j = 0; j < size1; j++)
								buff.append(derivation.get(i).translation
										.get(j).value
										+ (j + 1 < size1 ? "," : ""));
							buff.append(")");
						}
						if (i + 1 < size)
							buff.append(",");
					}
				}
				buff.append(HtmlUtils.br);
				outBuff.append(HtmlUtils.normal(buff.toString()));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.PICTURE)) {
			// word picture section
			if (derivation != null && picture.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_picture),
						FontColor.blue));
				outBuff.append("Picture English Picture Swedish" + HtmlUtils.br);
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.INFLECTION)) {
			// word inflection section
			if (inflection != null && inflection.size() > 0) {
				final StringBuffer buff = new StringBuffer();
				buff.append(HtmlUtils.title(
						res.getString(R.string.word_section_inflection),
						FontColor.black));
				final int size = inflection.size();
				for (int i = 0; i < size; i++) {
					String d_value = inflection.get(i).value;
					if (d_value != null && d_value.length() > 0) {
						buff.append(d_value);
						if (i + 1 < size)
							buff.append(",");
					}
				}
				buff.append(HtmlUtils.br);
				outBuff.append(HtmlUtils.normal(buff.toString()));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.SYNONYM)) {
			// word synonym section
			if (synonym != null && synonym.size() > 0) {
				final StringBuffer buff = new StringBuffer();
				buff.append(HtmlUtils.title(
						res.getString(R.string.word_section_synonym),
						FontColor.black));
				final int size = synonym.size();
				for (int i = 0; i < size; i++) {
					String d_value = synonym.get(i).value;
					if (d_value != null && d_value.length() > 0) {
						buff.append(d_value);
						if (i + 1 < size)
							buff.append(",");
					}
				}
				buff.append(HtmlUtils.br);
				outBuff.append(HtmlUtils.normal(buff.toString()));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.DEFINITION)) {
			// word definition section
			if (definition != null && definition.value != null
					&& definition.value.length() > 0) {
				final StringBuffer buff = new StringBuffer();
				buff.append(HtmlUtils.title(
						res.getString(R.string.word_section_definition),
						FontColor.black));
				buff.append(definition.value);

				if (definition.translation != null) {
					final int size = definition.translation.size();
					if (size > 0) {
						buff.append(" (");

						for (int i = 0; i < size; i++)
							if (definition.translation.get(i).value != null
									&& definition.translation.get(i).value
											.length() > 0)
								buff.append(definition.translation.get(i).value
										+ (i + 1 < size ? "," : ""));

						buff.append(")");
					}
				}

				buff.append(HtmlUtils.br);
				outBuff.append(HtmlUtils.normal(buff.toString()));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.USE)) {
			// word use section
			if (use != null && use.value != null && use.value.length() > 0)
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_use),
						FontColor.black) + HtmlUtils.normal(use.value));
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.EXAMPLE)) {
			// word example section
			if (example != null && example.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_example),
						FontColor.black));

				final StringBuffer buff = new StringBuffer();

				final int size = example.size();

				for (int i = 0; i < size; i++) {

					buff.append(example.get(i).value);

					if (example.get(i).translation != null
							&& example.get(i).translation.size() > 0) {
						int size1 = example.get(i).translation.size();

						if (size1 > 0)
							buff.append("(");

						for (int j = 0; j < size1; j++)
							if (example.get(i).translation.get(j).value != null
									&& example.get(i).translation.get(j).value
											.length() > 0)
								buff.append(example.get(i).translation.get(j).value
										+ (j + 1 < size1 ? "," : ""));

						buff.append(")");
					}
					if (i + 1 < size)
						buff.append("," + HtmlUtils.br);
				}

				outBuff.append(HtmlUtils.quote(HtmlUtils.normal(buff.toString())));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.EXPLANATION)) {
			// word explanation section
			if (explanation != null && explanation.value != null
					&& explanation.value.length() > 0) {

				final StringBuffer buff = new StringBuffer();
				buff.append(HtmlUtils.title(
						res.getString(R.string.word_section_explanation),
						FontColor.black));
				buff.append(explanation.value);

				if (explanation.translation != null) {
					final int size = explanation.translation.size();
					if (size > 0) {
						buff.append(" (");

						for (int i = 0; i < size; i++)
							if (explanation.translation.get(i).value != null
									&& explanation.translation.get(i).value
											.length() > 0)
								buff.append(explanation.translation.get(i).value
										+ (i + 1 < size ? "," : ""));

						buff.append(")");
					}
				}

				buff.append(HtmlUtils.br);
				outBuff.append(HtmlUtils.normal(buff.toString()));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.IDIOM)) {
			// word idiom section
			if (idiom != null && idiom.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_idiom),
						FontColor.black));

				final StringBuffer buff = new StringBuffer();

				final int size = idiom.size();

				for (int i = 0; i < size; i++) {

					buff.append(idiom.get(i).value);

					if (idiom.get(i).translation != null
							&& idiom.get(i).translation.size() > 0) {
						int size1 = idiom.get(i).translation.size();

						if (size1 > 0)
							buff.append("(");

						for (int j = 0; j < size1; j++)
							if (idiom.get(i).translation.get(j).value != null
									&& idiom.get(i).translation.get(j).value
											.length() > 0)
								buff.append(idiom.get(i).translation.get(j).value
										+ (j + 1 < size1 ? "," : ""));

						buff.append(")");
					}
					if (i + 1 < size)
						buff.append("," + HtmlUtils.br);
				}

				outBuff.append(HtmlUtils.quote(HtmlUtils.normal(buff.toString())));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.COMPOUND)) {
			// word compound section
			if (compound != null && compound.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_compound),
						FontColor.black));

				final StringBuffer buff = new StringBuffer();

				final int size = compound.size();

				for (int i = 0; i < size; i++) {

					buff.append(compound.get(i).value);

					if (compound.get(i).translation != null
							&& compound.get(i).translation.size() > 0) {
						int size1 = compound.get(i).translation.size();

						if (size1 > 0)
							buff.append("(");

						for (int j = 0; j < size1; j++)
							if (compound.get(i).translation.get(j).value != null
									&& compound.get(i).translation.get(j).value
											.length() > 0)
								buff.append(compound.get(i).translation.get(j).value
										+ (j + 1 < size1 ? "," : ""));

						buff.append(")");
					}
					if (i + 1 < size)
						buff.append("," + HtmlUtils.br);
				}

				outBuff.append(HtmlUtils.quote(HtmlUtils.normal(buff.toString())));
			}
		}

		if (options == null || options != null
				&& options.contains(PrintOptions.RELATED)) {
			// word related section
			if (related != null && related.size() > 0) {
				outBuff.append(HtmlUtils.title(
						res.getString(R.string.word_section_related),
						FontColor.black));

				final StringBuffer buff = new StringBuffer();

				final int size = related.size();

				for (int i = 0; i < size; i++) {

					buff.append(related.get(i).value);

					if (related.get(i).translation != null
							&& related.get(i).translation.size() > 0) {
						int size1 = related.get(i).translation.size();

						if (size1 > 0)
							buff.append("(");

						for (int j = 0; j < size1; j++)
							if (related.get(i).translation.get(j).value != null
									&& related.get(i).translation.get(j).value
											.length() > 0)
								buff.append(related.get(i).translation.get(j).value
										+ (j + 1 < size1 ? "," : ""));

						if (size1 > 0)
							buff.append(")");
					}
					if (i + 1 < size)
						buff.append("," + HtmlUtils.br);
				}

				outBuff.append(HtmlUtils.quote(HtmlUtils.normal(buff.toString())));
			}
		}

		// finally, append the footer to the output buffer
		if (footer != null && footer.length() > 0)
			outBuff.append(footer);

		return outBuff.toString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(value);

		dest.writeString(comment);

		dest.writeString(wordClass.name());

		dest.writeString(language.name());

		dest.writeInt(word_id);

		dest.writeTypedList(translation);

		dest.writeTypedList(see);

		dest.writeTypedList(derivation);

		dest.writeTypedList(picture);

		dest.writeTypedList(inflection);

		dest.writeTypedList(synonym);

		dest.writeTypedList(example);

		dest.writeTypedList(idiom);

		dest.writeTypedList(compound);

		dest.writeTypedList(related);

		dest.writeParcelable(grammar, flags);

		dest.writeParcelable(phonetic, flags);

		dest.writeParcelable(variant, flags);

		dest.writeParcelable(definition, flags);

		dest.writeParcelable(use, flags);

		dest.writeParcelable(explanation, flags);
	}
}

/**
 * This class provides a storage for word's compounds
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordCompound extends WordTranslations {

	public static final Parcelable.Creator<WordCompound> CREATOR = new Parcelable.Creator<WordCompound>() {
		@Override
		public WordCompound createFromParcel(Parcel in) {
			return new WordCompound(in);
		}

		@Override
		public WordCompound[] newArray(int size) {
			return new WordCompound[size];
		}
	};

	protected WordCompound(Parcel in) {
		super(in);
	}

	public WordCompound(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's definitions
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordDefinition extends WordTranslations {
	public static final Parcelable.Creator<WordDefinition> CREATOR = new Parcelable.Creator<WordDefinition>() {
		@Override
		public WordDefinition createFromParcel(Parcel in) {
			return new WordDefinition(in);
		}

		@Override
		public WordDefinition[] newArray(int size) {
			return new WordDefinition[size];
		}
	};

	public WordDefinition() {
		super("", 01);
	}

	public WordDefinition(Parcel in) {
		super(in);
	}

	public WordDefinition(String string, int id) {
		super(string, id);
	}

}

/**
 * This class provides a storage for word's derivations
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordDerivation extends WordTranslations {

	public static final Parcelable.Creator<WordDerivation> CREATOR = new Parcelable.Creator<WordDerivation>() {
		@Override
		public WordDerivation createFromParcel(Parcel in) {
			return new WordDerivation(in);
		}

		@Override
		public WordDerivation[] newArray(int size) {
			return new WordDerivation[size];
		}
	};

	public WordDerivation(Parcel in) {
		super(in);
	}

	public WordDerivation(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's examples
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordExample extends WordTranslations {

	public static final Parcelable.Creator<WordExample> CREATOR = new Parcelable.Creator<WordExample>() {
		@Override
		public WordExample createFromParcel(Parcel in) {
			return new WordExample(in);
		}

		@Override
		public WordExample[] newArray(int size) {
			return new WordExample[size];
		}
	};

	protected WordExample(Parcel in) {
		super(in);
	}

	public WordExample(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's explanations
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordExplanation extends WordTranslations {
	public static final Parcelable.Creator<WordExplanation> CREATOR = new Parcelable.Creator<WordExplanation>() {
		@Override
		public WordExplanation createFromParcel(Parcel in) {
			return new WordExplanation(in);
		}

		@Override
		public WordExplanation[] newArray(int size) {
			return new WordExplanation[size];
		}
	};

	public WordExplanation() {
		super("", -1);
	}

	protected WordExplanation(Parcel in) {
		super(in);
	}

	public WordExplanation(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's grammar
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordGrammar extends WordValue {
	public static final Parcelable.Creator<WordGrammar> CREATOR = new Parcelable.Creator<WordGrammar>() {
		@Override
		public WordGrammar createFromParcel(Parcel in) {
			return new WordGrammar(in);
		}

		@Override
		public WordGrammar[] newArray(int size) {
			return new WordGrammar[size];
		}
	};

	public WordGrammar() {
		super("", -1);
	}

	protected WordGrammar(Parcel in) {
		super(in);
	}

	public WordGrammar(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's idioms
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordIdiom extends WordTranslations {

	public static final Parcelable.Creator<WordIdiom> CREATOR = new Parcelable.Creator<WordIdiom>() {
		@Override
		public WordIdiom createFromParcel(Parcel in) {
			return new WordIdiom(in);
		}

		@Override
		public WordIdiom[] newArray(int size) {
			return new WordIdiom[size];
		}
	};

	protected WordIdiom(Parcel in) {
		super(in);
	}

	public WordIdiom(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for word's inflection
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordInflection extends WordValue {

	public static final Parcelable.Creator<WordInflection> CREATOR = new Parcelable.Creator<WordInflection>() {
		@Override
		public WordInflection createFromParcel(Parcel in) {
			return new WordInflection(in);
		}

		@Override
		public WordInflection[] newArray(int size) {
			return new WordInflection[size];
		}
	};

	protected WordInflection(Parcel in) {
		super(in);
	}

	public WordInflection(String string, int id) {
		super(string, id);
	}
}

/**
 * This class provides a storage for synonym's levels
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordLevel implements Parcelable {
	public byte minor;

	public byte major;

	public static final Parcelable.Creator<WordLevel> CREATOR = new Parcelable.Creator<WordLevel>() {
		@Override
		public WordLevel createFromParcel(Parcel in) {
			return new WordLevel(in);
		}

		@Override
		public WordLevel[] newArray(int size) {
			return new WordLevel[size];
		}
	};

	public WordLevel() {
	}

	protected WordLevel(Parcel in) {
		readFromParcel(in);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void readFromParcel(Parcel in) {
		in.writeByte(major);
		in.writeByte(minor);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte(major);
		dest.writeByte(minor);
	}

}

/**
 * This class provides a storage for word's phonetic
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordPhonetic extends WordValue {
	public static final Parcelable.Creator<WordPhonetic> CREATOR = new Parcelable.Creator<WordPhonetic>() {
		@Override
		public WordPhonetic createFromParcel(Parcel in) {
			return new WordPhonetic(in);
		}

		@Override
		public WordPhonetic[] newArray(int size) {
			return new WordPhonetic[size];
		}
	};

	public String soundFile;

	public WordPhonetic() {
		super("", -1);
		soundFile = "";
	}

	protected WordPhonetic(Parcel in) {
		super(in);
	}

	public WordPhonetic(String string, String string2, int id) {
		super(string, id);
		soundFile = string2;
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		soundFile = in.readString();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(soundFile);
	}
}

/**
 * This class provides a storage for word's related
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordRelated extends WordTranslations {

	public RelatedTypes type;

	public static final Parcelable.Creator<WordRelated> CREATOR = new Parcelable.Creator<WordRelated>() {
		@Override
		public WordRelated createFromParcel(Parcel in) {
			return new WordRelated(in);
		}

		@Override
		public WordRelated[] newArray(int size) {
			return new WordRelated[size];
		}
	};

	protected WordRelated(Parcel in) {
		super(in);
	}

	public WordRelated(String string, String string2, int id) {
		super(string, id);
		type = getRelatedType(string2);
	}

	private RelatedTypes getRelatedType(String string) {
		try {
			return RelatedTypes.valueOf(string);
		} catch (Exception e) {
			return RelatedTypes.UNKNOWN_TYPE;
		}
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		type = getRelatedType(in.readString());
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(type.name());
	}
}

/**
 * This class provides a storage for word's "see also"
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordSee extends WordValue {
	public SeeTypes type;

	public static final Parcelable.Creator<WordSee> CREATOR = new Parcelable.Creator<WordSee>() {
		@Override
		public WordSee createFromParcel(Parcel in) {
			return new WordSee(in);
		}

		@Override
		public WordSee[] newArray(int size) {
			return new WordSee[size];
		}
	};

	protected WordSee(Parcel in) {
		super(in);
	}

	public WordSee(String string, String string2, int id) {
		super(string, id);
		type = getSeeType(string2);
	}

	private SeeTypes getSeeType(String string) {
		try {
			return SeeTypes.valueOf(string);
		} catch (Exception e) {
			return SeeTypes.UNKNOWN_TYPE;
		}
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		type = getSeeType(in.readString());
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(type.name());
	}
}

/**
 * This class provides a storage for word's synonyms
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordSynonym extends WordValue {

	public WordLevel level;

	public static final Parcelable.Creator<WordSynonym> CREATOR = new Parcelable.Creator<WordSynonym>() {
		@Override
		public WordSynonym createFromParcel(Parcel in) {
			return new WordSynonym(in);
		}

		@Override
		public WordSynonym[] newArray(int size) {
			return new WordSynonym[size];
		}
	};

	protected WordSynonym(Parcel in) {
		super(in);
	}

	public WordSynonym(String string, byte minor, byte major, int id) {
		super(string, id);
		level = new WordLevel();
		level.major = major;
		level.minor = minor;
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		if (level == null)
			level = new WordLevel();
		level = in.readParcelable(WordLevel.class.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeParcelable(level, flags);
	}
}

class WordTranslation extends WordValueComment {

	public static final Parcelable.Creator<WordTranslation> CREATOR = new Parcelable.Creator<WordTranslation>() {
		@Override
		public WordTranslation createFromParcel(Parcel in) {
			return new WordTranslation(in);
		}

		@Override
		public WordTranslation[] newArray(int size) {
			return new WordTranslation[size];
		}
	};

	protected WordTranslation(Parcel in) {
		super(in);
	}

	public WordTranslation(String string, String string2, int id) {
		super(string, string2, id);
	}
}

/**
 * This class provides everything like WordValue plus a list of corresponding
 * value translation
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordTranslations extends WordValue {

	public List<WordTranslation> translation;

	public WordTranslations() {
		super("", -1);
		translation = new ArrayList<WordTranslation>();
	}

	protected WordTranslations(Parcel in) {
		super(in);
	}

	public WordTranslations(String string, int id) {
		super(string, id);
		translation = new ArrayList<WordTranslation>();
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		if (translation == null)
			translation = new ArrayList<WordTranslation>();
		in.readTypedList(translation, WordTranslation.CREATOR);
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeTypedList(translation);
	}
}

/**
 * This class provides a storage for word's pictures (and others in the future)
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordURL extends WordValue {
	public URLTypes type;

	public static final Parcelable.Creator<WordURL> CREATOR = new Parcelable.Creator<WordURL>() {
		@Override
		public WordURL createFromParcel(Parcel in) {
			return new WordURL(in);
		}

		@Override
		public WordURL[] newArray(int size) {
			return new WordURL[size];
		}
	};

	protected WordURL(Parcel in) {
		super(in);

	}

	public WordURL(String string, String string2, int id) {
		super(string, id);
		type = getURLType(string2);
	}

	private URLTypes getURLType(String string) {
		try {
			return URLTypes.valueOf(string);
		} catch (Exception e) {
			return URLTypes.UNKNOWN_TYPE;
		}
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		String s = in.readString();
		type = getURLType(s);
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(type.name());
	}
}

/**
 * This class provides a storage for word's "Use in..."
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordUse extends WordValue {
	public static final Parcelable.Creator<WordUse> CREATOR = new Parcelable.Creator<WordUse>() {
		@Override
		public WordUse createFromParcel(Parcel in) {
			return new WordUse(in);
		}

		@Override
		public WordUse[] newArray(int size) {
			return new WordUse[size];
		}
	};

	public WordUse() {
		super("", -1);
	}

	protected WordUse(Parcel in) {
		super(in);
	}

	public WordUse(String string, int id) {
		super(string, -1);
	}

}

/**
 * This class is used as an ancestor for any object that: - wants to be
 * parcelable - wnats to has an INT id and a TEXT value
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordValue implements Parcelable {

	public String value;
	public int id;

	public static final Parcelable.Creator<WordValue> CREATOR = new Parcelable.Creator<WordValue>() {
		@Override
		public WordValue createFromParcel(Parcel in) {
			return new WordValue(in);
		}

		@Override
		public WordValue[] newArray(int size) {
			return new WordValue[size];
		}
	};

	public WordValue() {
	}

	protected WordValue(Parcel in) {
		readFromParcel(in);
	}

	public WordValue(String value, int id) {
		this.value = value;
		this.id = id;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void readFromParcel(Parcel in) {
		id = in.readInt();
		value = in.readString();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(id);
		out.writeString(value);
	}
}

/**
 * This class provides everything like WordValue plus a TEXT comment
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordValueComment extends WordValue {

	public String comment;

	protected WordValueComment(Parcel in) {
		super(in);
	}

	public WordValueComment(String value, String comment, int id) {
		super(value, id);
		this.comment = comment == null ? "" : comment;
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		comment = in.readString();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(comment);
	}
}

/**
 * This class provides a storage for word's variants
 * 
 * @author "Eugen Mihailescu"
 * 
 */
class WordVariant extends WordValue {
	public static final Parcelable.Creator<WordVariant> CREATOR = new Parcelable.Creator<WordVariant>() {
		@Override
		public WordVariant createFromParcel(Parcel in) {
			return new WordVariant(in);
		}

		@Override
		public WordVariant[] newArray(int size) {
			return new WordVariant[size];
		}
	};

	public String alt;

	public WordVariant() {
		super("", -1);
		alt = "";
	}

	protected WordVariant(Parcel in) {
		super(in);
	}

	public WordVariant(String string, String string2, int id) {
		super(string, id);
		alt = string2;
	}

	@Override
	public void readFromParcel(Parcel in) {
		super.readFromParcel(in);
		alt = in.readString();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		super.writeToParcel(out, flags);
		out.writeString(alt);
	}
}
