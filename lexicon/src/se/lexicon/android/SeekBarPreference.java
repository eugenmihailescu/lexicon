package se.lexicon.android;

/**
 * ----------------------------------------------------------------------------
 * This file is part of lexicon.
 * 
 * Copyright 2012 "Eugen Mihailescu"
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	1.1-rc-1-ga23412c $
 * @commit:		a23412c664627c4fde66b89a236aade84c3a38da $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun May 20 12:18:46 2012 +0200 $
 * @file:		SeekBarPreference.java $
 * 
 * @id:	SeekBarPreference.java | Sun May 20 12:18:46 2012 +0200 | Eugen Mihailescu  $
 * 
 */

//http://android-journey.blogspot.se/2010/01/for-almost-any-application-we-need-to.html
//http://robobunny.com/wp/2011/08/13/android-seekbar-preference/
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

/**
 * Class used to display the application preference screen
 * 
 */
public class SeekBarPreference extends Preference implements
		OnSeekBarChangeListener {

	private final String TAG = getClass().getName();

	private static final String ANDROID_NAMESPACE = "http://schemas.android.com/apk/res/android";
	private static final String SEEKBAR_NAMESPACE = "http://se.lexicon.android";
	private static final int DEFAULT_VALUE = 10;

	private int mMaxValue = Short.MAX_VALUE;
	private int mMinValue = 10;
	private int mInterval = 10;
	private int mCurrentValue;
	private String mUnitsLeft = "";
	private String mUnitsRight = "";
	private String mUnitsFormat = "";
	private String mSummary = "";
	private SeekBar mSeekBar;

	private TextView mStatusText;
	private TextView txtSummary;

	/**
	 * Constructor that is called when inflating a Preference from XML. This is
	 * called when a Preference is being constructed from an XML file, supplying
	 * attributes that were specified in the XML file. This version uses a
	 * default style of 0, so the only attribute values applied are those in the
	 * Context's Theme and the given AttributeSet.
	 * 
	 * @param context
	 *            The Context this is associated with, through which it can
	 *            access the current theme, resources, {@link SharedPreferences}
	 *            , etc.
	 * @param attrs
	 *            The attributes of the XML tag that is inflating the
	 *            preference.
	 * @see #Preference(Context, AttributeSet, int)
	 */
	public SeekBarPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPreference(context, attrs);
	}

	/**
	 * Perform inflation from XML and apply a class-specific base style. This
	 * constructor of Preference allows subclasses to use their own base style
	 * when they are inflating. For example, a {@link CheckBoxPreference}
	 * constructor calls this version of the super class constructor and
	 * supplies {@code android.R.attr.checkBoxPreferenceStyle} for
	 * <var>defStyle</var>. This allows the theme's checkbox preference style to
	 * modify all of the base preference attributes as well as the
	 * {@link CheckBoxPreference} class's attributes.
	 * 
	 * @param context
	 *            The Context this is associated with, through which it can
	 *            access the current theme, resources, {@link SharedPreferences}
	 *            , etc.
	 * @param attrs
	 *            The attributes of the XML tag that is inflating the
	 *            preference.
	 * @param defStyle
	 *            The default style to apply to this preference. If 0, no style
	 *            will be applied (beyond what is included in the theme). This
	 *            may either be an attribute resource, whose value will be
	 *            retrieved from the current theme, or an explicit style
	 *            resource.
	 * @see #Preference(Context, AttributeSet)
	 */
	public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initPreference(context, attrs);
	}

	/**
	 * Return a string value of a specified attribute from a specified namespace
	 * based on its name
	 * 
	 * @param attrs
	 *            the AttributeSet from which to read the value
	 * @param namespace
	 *            the namespace that owns the attribute
	 * @param name
	 *            the name of the attribute to be obtained
	 * @param defaultValue
	 *            the value that will be used if the attribute is not found on
	 *            the namespace
	 * @return a string containing the value of the read attribute
	 */
	private String getAttributeStringValue(AttributeSet attrs,
			String namespace, String name, String defaultValue) {
		String value = attrs.getAttributeValue(namespace, name);
		if (value == null)
			value = defaultValue;

		return value;
	}

	/**
	 * Sets the internal private variables with the values obtained from the
	 * attrs
	 * 
	 * @param context
	 *            the context where the seek bar control will be shown
	 * @param attrs
	 *            the attributes retrieved from the preferences Map that will be
	 *            used and shown in the preference screen
	 */
	private void initPreference(Context context, AttributeSet attrs) {
		setValuesFromXml(attrs);
		mSeekBar = new SeekBar(context, attrs);
		mSeekBar.setMax(mMaxValue - mMinValue);
		mSeekBar.setOnSeekBarChangeListener(this);
	}

	@Override
	public void onBindView(View view) {
		super.onBindView(view);

		try {
			// move our seekbar to the new view we've been given
			ViewParent oldContainer = mSeekBar.getParent();
			ViewGroup newContainer = (ViewGroup) view
					.findViewById(R.id.seekBarPrefBarContainer);

			if (oldContainer != newContainer) {
				// remove the seekbar from the old view
				if (oldContainer != null) {
					((ViewGroup) oldContainer).removeView(mSeekBar);
				}
				// remove the existing seekbar (there may not be one) and add
				// ours
				newContainer.removeAllViews();
				newContainer.addView(mSeekBar,
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
			}
		} catch (Exception ex) {
			Log.e(TAG, "Error binding view: " + ex.toString());
		}

		updateView(view);
	}

	@Override
	protected View onCreateView(ViewGroup parent) {

		RelativeLayout layout = null;

		try {
			LayoutInflater mInflater = (LayoutInflater) getContext()
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			layout = (RelativeLayout) mInflater.inflate(
					R.layout.seekbarpreference, parent, false);
		} catch (Exception e) {
			Log.e(TAG, "Error creating seek bar preference", e);
		}

		return layout;

	}

	@Override
	protected Object onGetDefaultValue(TypedArray ta, int index) {

		int defaultValue = ta.getInt(index, DEFAULT_VALUE);
		return defaultValue;

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		int newValue = progress + mMinValue;

		if (newValue > mMaxValue)
			newValue = mMaxValue;
		else if (newValue < mMinValue)
			newValue = mMinValue;
		else if (mInterval != 1 && newValue % mInterval != 0)
			newValue = Math.round(((float) newValue) / mInterval) * mInterval;

		// change rejected, revert to the previous value
		if (!callChangeListener(newValue)) {
			seekBar.setProgress(mCurrentValue - mMinValue);
			return;
		}

		// change accepted, store it
		mCurrentValue = newValue;
		if (mUnitsFormat.length() > 0) {
			txtSummary.setText(String.format(mUnitsFormat, mCurrentValue));
			mStatusText.setVisibility(View.INVISIBLE);
		} else {
			mStatusText.setVisibility(View.VISIBLE);
			mStatusText.setText(String.valueOf(mCurrentValue));
			txtSummary.setText(mSummary);
		}
		persistInt(newValue);

	}

	@Override
	protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {

		if (restoreValue) {
			mCurrentValue = getPersistedInt(mCurrentValue);
		} else {
			int temp = 0;
			try {
				temp = (Integer) defaultValue;
			} catch (Exception ex) {
				Log.e(TAG, "Invalid default value: " + defaultValue.toString());
			}

			persistInt(temp);
			mCurrentValue = temp;
		}

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		notifyChanged();
	}

	/**
	 * Initialize the internal used variables with the values of those
	 * attributes read from the shared preference file
	 * 
	 * @param attrs
	 *            the AttributeSet where are stored (after parsing the XML
	 *            preference file) the user saved preferences
	 */
	private void setValuesFromXml(AttributeSet attrs) {
		mMaxValue = attrs.getAttributeIntValue(ANDROID_NAMESPACE, "max", 200);
		mMinValue = attrs.getAttributeIntValue(SEEKBAR_NAMESPACE, "min", 10);

		mSummary = getAttributeStringValue(attrs, SEEKBAR_NAMESPACE, "summary",
				"");

		int id = getContext().getResources().getIdentifier(mSummary, "id",
				getContext().getPackageName());
		if (id > 0)
			mSummary = getContext().getResources().getString(id);

		mUnitsLeft = getAttributeStringValue(attrs, SEEKBAR_NAMESPACE,
				"unitsLeft", "");
		String units = getAttributeStringValue(attrs, SEEKBAR_NAMESPACE,
				"units", "");
		mUnitsRight = getAttributeStringValue(attrs, SEEKBAR_NAMESPACE,
				"unitsRight", units);
		mUnitsFormat = getAttributeStringValue(attrs, SEEKBAR_NAMESPACE,
				"unitFormat", "");
		id = getContext().getResources().getIdentifier(mUnitsFormat, "id",
				getContext().getPackageName());
		if (id > 0)
			mUnitsFormat = getContext().getResources().getString(id);

		try {
			String newInterval = attrs.getAttributeValue(SEEKBAR_NAMESPACE,
					"interval");
			if (newInterval != null)
				mInterval = Integer.parseInt(newInterval);
		} catch (Exception e) {
			Log.e(TAG, "Invalid interval value", e);
		}

	}

	/**
	 * Update a SeekBarPreference view with our current state
	 * 
	 * @param view
	 */
	protected void updateView(View view) {

		try {
			RelativeLayout layout = (RelativeLayout) view;

			mStatusText = (TextView) layout.findViewById(R.id.seekBarPrefValue);
			TextView unitsRight = (TextView) layout
					.findViewById(R.id.seekBarPrefUnitsRight);
			TextView unitsLeft = (TextView) layout
					.findViewById(R.id.seekBarPrefUnitsLeft);
			txtSummary = (TextView) layout
					.findViewById(R.id.seekBarPrefUnitFormat);

			if (mUnitsFormat.length() > 0) {
				txtSummary.setText(String.format(mUnitsFormat, mCurrentValue));
				mStatusText.setVisibility(View.INVISIBLE);
			} else {
				mStatusText.setVisibility(View.VISIBLE);
				mStatusText.setText(String.valueOf(mCurrentValue));
				txtSummary.setText(mSummary);
			}

			mStatusText.setMinimumWidth(30);

			mSeekBar.setProgress(mCurrentValue - mMinValue);

			unitsRight.setText(mUnitsRight);

			unitsLeft.setText(mUnitsLeft);

		} catch (Exception e) {
			Log.e(TAG, "Error updating seek bar preference", e);
		}

	}

}